package usecase

import (
	"context"
	"mymodule/model"
)

func (uc *usecase) RoleAll(ctx context.Context, req model.GetAllReq) ([]model.Role, int, error) {

	return uc.roleDB.Get(ctx, req)
}

func (uc *usecase) RoleGetCode(ctx context.Context) ([]model.DataMasterCode, error) {

	return uc.roleDB.GetCode(ctx)
}

func (uc *usecase) RoleByID(ctx context.Context, ID int64) (model.Role, error) {

	return uc.roleDB.GetDetail(ctx, ID)
}

func (uc *usecase) RoleCreate(ctx context.Context, body model.Role) error {
	return uc.roleDB.Create(ctx, body)
}

func (uc *usecase) RoleUpdate(ctx context.Context, body model.Role) error {
	return uc.roleDB.Update(ctx, body)
}

func (uc *usecase) RoleDelete(ctx context.Context, ID int64) error {
	return uc.roleDB.Delete(ctx, ID)
}
