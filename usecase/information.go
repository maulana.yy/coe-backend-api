package usecase

import (
	"context"
	"mymodule/model"
)

func (uc *usecase) InformationAll(ctx context.Context, req model.GetAllReq) ([]model.Information, int, error) {

	return uc.informationDB.Get(ctx, req)
}

func (uc *usecase) InformationByID(ctx context.Context, ID int64) (model.Information, error) {

	return uc.informationDB.GetDetail(ctx, ID)
}

func (uc *usecase) InformationCreate(ctx context.Context, body model.Information) error {
	return uc.informationDB.Create(ctx, body)
}

func (uc *usecase) InformationUpdate(ctx context.Context, body model.Information) error {
	return uc.informationDB.Update(ctx, body)
}

func (uc *usecase) InformationDelete(ctx context.Context, ID int64) error {
	return uc.informationDB.Delete(ctx, ID)
}


func (uc *usecase) InformationChangeStatus(ctx context.Context, ID int64) error {

	return uc.informationDB.ChangeStatus(ctx, ID)
}
