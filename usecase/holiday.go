package usecase

import (
	"context"
	"mymodule/model"
	"time"

	"github.com/lib/pq"
)

func (uc *usecase) HolidayAll(ctx context.Context, req model.GetAllReq) ([]model.HolidayRes, int, error) {

	var result []model.HolidayRes
	data, count, err := uc.holidayDB.Get(ctx, req)

	if err != nil {
		return []model.HolidayRes{}, 0, err
	}

	for _, dt := range data {

		result = append(result, model.HolidayRes{
			ID:          dt.ID,
			Name:        dt.Name,
			Category:    dt.Category,
			HolidayDate: dt.HolidayDate.Format("2006-01-02"),
		})
	}

	return result, count, nil
}

func (uc *usecase) HolidayByID(ctx context.Context, ID int64) (model.Holiday, error) {

	return uc.holidayDB.GetDetail(ctx, ID)
}

func (uc *usecase) HolidayCreate(ctx context.Context, body model.HolidayReq) error {

	day, err := time.Parse("2006-01-02", body.HolidayDate)

	if err != nil {
		return err
	}

	holiday := model.Holiday{
		Name:        body.Name,
		HolidayDate: day,
		Category:    body.Category,
		CreatedBy:   body.CreatedBy,
	}

	return uc.holidayDB.Create(ctx, holiday)
}

func (uc *usecase) HolidayUpdate(ctx context.Context, body model.HolidayReq) error {
	day, err := time.Parse("2006-01-02", body.HolidayDate)

	if err != nil {
		return err
	}

	holiday := model.Holiday{
		ID:          body.ID,
		Name:        body.Name,
		Category:    body.Category,
		HolidayDate: day,
		UpdatedAt: pq.NullTime{
			Time:  time.Now(),
			Valid: true,
		},
	}
	return uc.holidayDB.Update(ctx, holiday)
}

func (uc *usecase) HolidayDelete(ctx context.Context, ID int64) error {
	return uc.holidayDB.Delete(ctx, ID)
}
