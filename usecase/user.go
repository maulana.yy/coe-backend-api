package usecase

import (
	"context"
	"crypto/sha256"
	"errors"
	"fmt"
	"mymodule/model"
	"strconv"
	"time"

	"golang.org/x/crypto/bcrypt"
)

func (uc *usecase) Login(ctx context.Context, req model.Login) (model.User, error) {

	user, err := uc.userDB.GetByUsername(ctx, req.Username)

	if err != nil {
		fmt.Println("ERROR GET BY USERNAME")
		// if req.Username == "superadmin" {
		// 	hashPassword := []byte(string(req.Password))
		// 	hashPassword, err := bcrypt.GenerateFromPassword(hashPassword, bcrypt.DefaultCost)
		// 	if err != nil {
		// 		return model.User{}, err
		// 	}

		// 	payload := model.User{
		// 		Username: req.Username,
		// 		Password: string(hashPassword),
		// 		Email:    "superadmin@gmail.com",
		// 	}

		// 	err = uc.userDB.Create(ctx, &payload)

		// 	if err != nil {
		// 		return model.User{}, err
		// 	}

		// } else {
		return model.User{}, err
		// }
	}

	if err = bcrypt.CompareHashAndPassword(
		[]byte(user.Password),
		[]byte(req.Password)); err != nil {
		return model.User{}, errors.New("Password does not match..")
	}

	generatedTime, token := generateToken(user.ID, user.Password)
	user.Info.TokenGeneratedTime, user.Token = generatedTime.Unix(), token

	return user, nil
}

func (uc *usecase) UserAll(ctx context.Context, req model.GetAllReq) ([]model.UserInfo, int, error) {

	var data []model.UserInfo
	users, count, err := uc.userDB.Get(ctx, req)

	if err != nil {
		return data, 0, err
	}

	for _, user := range users {

		data = append(data, model.UserInfo{
			ID:         user.ID,
			Username:   user.Username,
			Email:      user.Email,
			Name:       user.Info.Name,
			Role:       user.Info.Role,
			Department: user.Info.Department,
			Level:      user.Info.Level,
			CreatedAt:  user.CreatedAt,
		})
	}

	return data, count, nil
}

func (uc *usecase) UserGetByID(ctx context.Context, userID int64) (*model.User, error) {

	return uc.userDB.GetDetail(ctx, userID)
}

func (uc *usecase) UserGetCode(ctx context.Context, department []string) ([]model.UserCode, error) {

	return uc.userDB.GetCode(ctx, department)
}

func (uc *usecase) UserCreate(ctx context.Context, body model.User) error {

	hashPassword := []byte(string(body.Password))
	hashPassword, err := bcrypt.GenerateFromPassword(hashPassword, bcrypt.DefaultCost)
	if err != nil {
		return err
	}

	body.Password = string(hashPassword)

	return uc.userDB.Create(ctx, &body)
}

func (uc *usecase) UserUpdate(ctx context.Context, body model.UserUpdate) error {

	return uc.userDB.Update(ctx, body)
}

func (uc *usecase) UserDelete(ctx context.Context, userID int64) error {
	return uc.userDB.Delete(ctx, userID)
}

func (uc *usecase) UserUpdateInfo(ctx context.Context, info model.UserUpdateInfoReq) (*model.User, error) {
	return &model.User{}, nil
}

func generateToken(userID int64, userPassword string) (generatedTIme time.Time, token string) {
	now := time.Now()
	h := sha256.New()
	c := strconv.FormatInt(userID, 10) + userPassword + strconv.FormatInt(now.Unix(), 10)
	h.Write([]byte(c))
	token = fmt.Sprintf("%x", h.Sum(nil))

	return now, token
}
