package usecase

import (
	"context"
	"encoding/csv"
	"errors"
	"fmt"
	"log"
	"mymodule/model"
	"os"
	"strings"
)

func (uc *usecase) ParticipantCreate(ctx context.Context, data model.Participant) error {
	return uc.participantDB.Create(ctx, &data)
}

func (uc *usecase) ParticipantUpdate(ctx context.Context, data model.Participant) error {
	return uc.participantDB.Update(ctx, data)
}

func (uc *usecase) ParticipantDelete(ctx context.Context, ID int64) error {
	return uc.participantDB.Delete(ctx, ID)
}

func (uc *usecase) ParticipantGet(ctx context.Context, data model.GetAllReq) ([]model.Participant, int, error) {
	return uc.participantDB.Get(ctx, data)
}

func (uc *usecase) ParticipantGetDetail(ctx context.Context, ID int64) (*model.Participant, error) {
	return uc.participantDB.GetDetail(ctx, ID)
}

func (uc *usecase) InsertBulkCSV(ctx context.Context, filename string) error {

	fmt.Println("INSER BULK CSV")
	file, err := os.Open("public/excel/" + filename)
	if err != nil {
		return err
	}
	defer file.Close()

	records, err := csv.NewReader(file).ReadAll()
	if err != nil {
		fmt.Println("ERROR PARTICIPANT : ", err.Error())
		return errors.New("ada yang salah")
	}

	fmt.Println("LENGTH RECORD : ", len(records))
	for idx, record := range records {
		fmt.Println("INDEX : ", idx)
		if idx > 0 && record[4] != "" {
			fmt.Println(record)
			fmt.Println("EMAIL : ", record[4])
			fmt.Println("DEPARTMENT : ", record[2])
			fmt.Println("LEVEL : ", record[5])
			participant, _ := uc.participantDB.GetDByEmail(ctx, record[4])

			organizer, err := uc.organizerDB.GetByCode(ctx, strings.ToUpper(record[2]))
			if err != nil {
				log.Println("ERROR GET ORGANIZER : ", err.Error())
				return err
			}
			level, err := uc.roleDB.GetByName(ctx, record[5])
			if err != nil {
				log.Println("ERROR GET LEVEL : ", err.Error())

				return err
			}
			if organizer.Code != "" && level.Name != "" && participant == nil {
				data := model.Participant{
					Name:      record[1],
					Email:     record[4],
					Organizer: record[2],
					Level:     strings.ToUpper(record[5]),
				}

				fmt.Println("DATA")
				fmt.Println(data)
				err = uc.participantDB.Create(ctx, &data)
				if err != nil {
					return err
				}
			}
		}

	}

	return nil

}

func (uc *usecase) ParticipantGetCode(ctx context.Context, organizer, level string) ([]model.ParticipantCode, error) {
	return uc.participantDB.GetCode(ctx, organizer, level)
}
