package usecase

import (
	"context"
	"mymodule/model"
)

func (uc *usecase) DicAll(ctx context.Context, req model.GetAllReq) ([]model.Dic, int, error) {

	return uc.dicDB.Get(ctx, req)
}

func (uc *usecase) DicGetCode(ctx context.Context) ([]model.DataMasterCodeOrganizerDic, error) {

	return uc.dicDB.GetCode(ctx)
}

func (uc *usecase) DicByID(ctx context.Context, ID int64) (model.Dic, error) {

	return uc.dicDB.GetDetail(ctx, ID)
}

func (uc *usecase) DicCreate(ctx context.Context, body model.Dic) error {
	return uc.dicDB.Create(ctx, body)
}

func (uc *usecase) DicUpdate(ctx context.Context, body model.Dic) error {
	return uc.dicDB.Update(ctx, body)
}

func (uc *usecase) DicDelete(ctx context.Context, ID int64) error {
	return uc.dicDB.Delete(ctx, ID)
}
