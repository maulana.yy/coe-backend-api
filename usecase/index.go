package usecase

import (
	"mymodule/app"
	"mymodule/db"
	"mymodule/model"
)

type usecase struct {
	userDB        db.User
	categoryDB    db.Category
	organizerDB   db.Organizer
	roleDB        db.Role
	eventDB       db.Event
	holidayDB     db.Holiday
	supportDB     db.Support
	informationDB db.Information
	dicDB         db.Dic
	participantDB db.Participant
	cfg           *model.Config
}

func NewUsecase(repo *app.Repo, cfg *model.Config) app.Usecase {
	return &usecase{
		userDB:        repo.UserDB,
		categoryDB:    repo.CategoryDB,
		organizerDB:   repo.OrganizerDB,
		roleDB:        repo.RoleDB,
		eventDB:       repo.EventDB,
		holidayDB:     repo.HolidayDB,
		supportDB:     repo.SupportDB,
		informationDB: repo.InformationDB,
		dicDB:         repo.DicDB,
		participantDB: repo.ParticipantDB,
		cfg:           cfg,
	}
}
