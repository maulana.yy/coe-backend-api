package usecase

import (
	"context"
	"mymodule/model"
)

func (uc *usecase) SupportAll(ctx context.Context, req model.GetAllReq) ([]model.Support, int, error) {

	return uc.supportDB.Get(ctx, req)
}

func (uc *usecase) SupportGetCode(ctx context.Context) ([]model.DataMasterCode, error) {

	return uc.supportDB.GetCode(ctx)
}

func (uc *usecase) SupportByID(ctx context.Context, ID int64) (model.Support, error) {

	return uc.supportDB.GetDetail(ctx, ID)
}

func (uc *usecase) SupportCreate(ctx context.Context, body model.Support) error {
	return uc.supportDB.Create(ctx, body)
}

func (uc *usecase) SupportUpdate(ctx context.Context, body model.Support) error {
	return uc.supportDB.Update(ctx, body)
}

func (uc *usecase) SupportDelete(ctx context.Context, ID int64) error {
	return uc.supportDB.Delete(ctx, ID)
}
