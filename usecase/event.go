package usecase

import (
	"context"
	"fmt"
	"mymodule/model"
	"strings"
	"time"

	"github.com/xuri/excelize/v2"
)

func (uc *usecase) EventAll(ctx context.Context, req model.GetAllReq) ([]model.EventInfo, int, error) {

	return uc.eventDB.Get(ctx, req)
}

func (uc *usecase) EventByID(ctx context.Context, ID int64) (model.Event, error) {

	return uc.eventDB.GetDetail(ctx, ID)
}

func (uc *usecase) EventCreate(ctx context.Context, body model.EventRequest) error {
	var (
		event        model.Event
		participants []model.ParticipantEvent
		attachments  []model.Attachment
	)

	layoutFormat := "2006-01-02"
	startDate, _ := time.Parse(layoutFormat, body.StartDate)
	endDate, _ := time.Parse(layoutFormat, body.EndDate)

	for _, user := range body.Participant {

		userList := strings.Split(user, "-")

		participants = append(participants, model.ParticipantEvent{
			Username: userList[1],
			Name:     userList[0],
		})
	}
	fmt.Println("==========")
	fmt.Println(participants)
	fmt.Println("==========")

	attachments = append(attachments, model.Attachment{
		Type: "",
		Path: body.Attachment,
	})

	fmt.Println("==========")
	fmt.Println(attachments)
	fmt.Println("==========")

	event = model.Event{
		Name: body.Name,
		Category: model.CategoryEvent{
			Name: body.Category,
			Room: body.Room,
			Link: body.Link,
		},
		Organizer:  body.Organizer,
		Department: body.Department,
		Level:      body.Level,
		Support: model.SupportEvent{
			Pic:      body.Pic,
			ListItem: body.Support,
		},
		Participant:    participants,
		Status:         "Scheduled",
		Approved:       "PENDING",
		Duration:       0,
		Note:           body.Note,
		Attachment:     attachments,
		EventDateStart: startDate,
		EventDateEnd:   endDate,
		EventTimeStart: body.StartTime,
		EventTimeEnd:   body.EndTime,
		CreatedBy:      body.Createdby,
	}
	return uc.eventDB.Create(ctx, event)
}

func (uc *usecase) EventUpdate(ctx context.Context, body model.EventRequest, id int64) error {
	var (
		event        model.Event
		participants []model.ParticipantEvent
		attachments  []model.Attachment
	)

	for _, user := range body.Participant {
		userList := strings.Split(user, "-")

		participants = append(participants, model.ParticipantEvent{
			Username: userList[1],
			Name:     userList[0],
		})
	}

	attachments = append(attachments, model.Attachment{
		Type: "",
		Path: body.Attachment,
	})

	event = model.Event{
		ID:   id,
		Name: body.Name,
		Category: model.CategoryEvent{
			Name: body.Category,
			Room: body.Room,
			Link: body.Link,
		},
		Organizer:  body.Organizer,
		Department: body.Department,
		Level:      body.Level,
		Support: model.SupportEvent{
			Pic:      body.Pic,
			ListItem: body.Support,
		},
		Participant: participants,
		Note:        body.Note,
		Attachment:  attachments,
	}

	return uc.eventDB.Update(ctx, event)
}

func (uc *usecase) EventDelete(ctx context.Context, ID int64) error {
	return uc.eventDB.Delete(ctx, ID)
}

func (uc *usecase) EventGetReport(ctx context.Context) (string, error) {
	f := excelize.NewFile()

	values := make(map[string]string)

	data, _, err := uc.eventDB.Get(ctx, model.GetAllReq{})

	if err != nil {
		fmt.Println("ERROR GET DATA : ", err)
		return "", err
	}

	style, err := f.NewStyle(&excelize.Style{
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
	})

	if err != nil {
		fmt.Println("ERROR CREATE STYLE")
		return "", err
	}

	label := map[string]string{
		"A1": "No", "B1": "Name", "C1": "Category",
		"D1": "Organizer", "E1": "Department", "F1": "Event Date", "G1": "Statu	s"}
	for x, dt := range data {

		values[fmt.Sprintf("A%d", x+2)] = fmt.Sprintf("%d", x+1)
		values[fmt.Sprintf("B%d", x+2)] = dt.Name
		values[fmt.Sprintf("C%d", x+2)] = dt.Category
		values[fmt.Sprintf("D%d", x+2)] = dt.Organizer
		values[fmt.Sprintf("E%d", x+2)] = strings.Join(dt.Department, ",")
		values[fmt.Sprintf("F%d", x+2)] = dt.EventDateStart.Format("2006-01-02")
		values[fmt.Sprintf("G%d", x+2)] = dt.Status

		f.SetCellStyle("Sheet1", fmt.Sprintf("A%d", x+2), fmt.Sprintf("G%d", x+2), style)

	}

	err = f.SetCellStyle("Sheet1", "A1", "G1", style)

	if err != nil {
		fmt.Println("ERROR ADD STYLE")
	}
	for k, v := range label {
		f.SetCellValue("Sheet1", k, v)
	}
	for k, v := range values {
		f.SetCellValue("Sheet1", k, v)
	}

	// Save spreadsheet by 	the given path.
	if err := f.SaveAs("Book1.xlsx"); err != nil {
		fmt.Println("ERROR SAVE")
		fmt.Println(err)
	}
	return "Book1.xlsx", nil
}

func (uc *usecase) EventApprove(ctx context.Context, ID int64, approval string) error {

	return uc.eventDB.UpdateApproval(ctx, ID, approval)
}

func (uc *usecase) EventUpdateStatus(ctx context.Context, ID int64, status, newDateStart, newDateEnd, newTimeStart, newTimeEnd string) error {

	return uc.eventDB.UpdateStatus(ctx, ID, status, newDateStart, newDateEnd, newTimeStart, newTimeEnd)
}

func (uc *usecase) EventToday(ctx context.Context) (model.EventDetail, error) {

	data, err := uc.eventDB.Today(ctx)

	fmt.Println("LENGTH EVENT TODAY : ", len(data))
	if len(data) == 0 || err != nil {
		return model.EventDetail{}, nil
	}

	// pic, err := uc.userDB.GetByUsername(ctx, data[0].Support.Pic)

	// if err != nil {
	// 	return model.EventDetail{}, nil
	// }
	date := data[0].EventDateStart.Format("2006-01-02")
	event := model.EventDetail{
		ID:             data[0].ID,
		Name:           data[0].Name,
		Organizer:      data[0].Organizer,
		Participant:    data[0].Participant,
		Status:         data[0].Status,
		Approved:       data[0].Approved,
		EventDateStart: date,
		EventTimeStart: data[0].EventTimeStart,
		EventTimeEnd:   data[0].EventTimeEnd,
		// PicUsername:    pic.Username,
		// PicName:        pic.Info.Name,
		// PicRole:        pic.Info.Role,
	}

	return event, err
}

func (uc *usecase) EventOfCalendar(ctx context.Context, calendar, startDate, endDate string) ([]model.EventTime, error) {

	var (
		events []model.EventTime
		index  int
	)
	_, week := time.Now().ISOWeek()
	month := int(time.Now().Month())

	if calendar == "week" {
		index = week
	}

	if calendar == "month" {
		index = month
	}
	data, err := uc.eventDB.GetCalendar(ctx, calendar, startDate, endDate, index)
	dataHoliday, err := uc.holidayDB.GetCalendar(ctx, calendar, startDate, endDate, index)
	if err != nil {
		return events, err
	}
	fmt.Println("LENGTH EVENT : ", len(data))
	for _, dt := range data {

		dateStart := dt.EventDateStart.Format("2006-01-02")
		dateEnd := dt.EventDateEnd.Format("2006-01-02")

		dateStart += " " + dt.EventTimeStart
		dateEnd += " " + dt.EventTimeEnd

		events = append(events, model.EventTime{
			Title: dt.Name,
			Start: dateStart,
			End:   dateEnd,
			Color: dt.Color,
		})
	}
	for _, dt := range dataHoliday {
		dateStart := dt.HolidayDate.Format("2006-01-02")
		dateEnd := dt.HolidayDate.Format("2006-01-02")

		dateStart += " 00:00:00"
		dateEnd += " 23:59:59"

		events = append(events, model.EventTime{
			Title: dt.Name,
			Start: dateStart,
			End:   dateEnd,
			Color: dt.Color,
		})
	}

	fmt.Println("LENGTH EVENTS : ", len(events))

	return events, nil
}
