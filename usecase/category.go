package usecase

import (
	"context"
	"errors"
	"mymodule/model"
)

func (uc *usecase) CategoryAll(ctx context.Context, req model.GetAllReq) ([]model.Category, int, error) {

	return uc.categoryDB.Get(ctx, req)
}

func (uc *usecase) CategoryGetCode(ctx context.Context) ([]model.DataMasterCode, error) {

	return uc.categoryDB.GetCode(ctx)
}

func (uc *usecase) CategoryByID(ctx context.Context, ID int64) (model.Category, error) {

	return uc.categoryDB.GetDetail(ctx, ID)
}

func (uc *usecase) CategoryCreate(ctx context.Context, body model.Category) error {

	category, _ := uc.categoryDB.GetByName(ctx, body.Name)

	if category.Name != "" {
		return errors.New("Category Has Been Created")
	}

	return uc.categoryDB.Create(ctx, body)
}

func (uc *usecase) CategoryUpdate(ctx context.Context, body model.Category) error {
	return uc.categoryDB.Update(ctx, body)
}

func (uc *usecase) CategoryDelete(ctx context.Context, ID int64) error {
	return uc.categoryDB.Delete(ctx, ID)
}
