package usecase

import (
	"context"
	"mymodule/model"
	"time"
)

func (uc *usecase) Calendar(ctx context.Context) ([]model.Event, error) {

	return []model.Event{}, nil
}

func (uc *usecase) GetInfo(ctx context.Context) ([]model.Information, error) {

	data, _, err := uc.informationDB.GetInfoDashboard(ctx)

	return data, err
}

func (uc *usecase) GetEventToday(ctx context.Context) ([]model.EventInfo, error) {

	tomorrow := time.Now().AddDate(0, 0, 1).Format("2006-01-02")

	req := model.GetAllReq{
		Offset:    1,
		Limit:     5,
		Search:    "",
		StartDate: time.Now().Format("2006-01-02"),
		EndDate:   tomorrow,
	}

	data, _, err := uc.eventDB.Get(ctx, req)

	return data, err
}

func (uc *usecase) GroupingEvent(ctx context.Context) ([]model.GroupEvent, error) {

	data := make([]model.GroupEvent, 0)

	Success, err := uc.eventDB.CountByStatus(ctx, "Success", 0)
	if err != nil {
		return data, err
	}
	data = append(data, model.GroupEvent{
		Name:  "success",
		Count: Success,
	})

	Reschedule, err := uc.eventDB.CountByStatus(ctx, "Reschedule", 0)
	if err != nil {
		return data, err
	}
	data = append(data, model.GroupEvent{
		Name:  "reschedule",
		Count: Reschedule,
	})

	Failed, err := uc.eventDB.CountByStatus(ctx, "Cancel", 0)
	if err != nil {
		return data, err
	}
	data = append(data, model.GroupEvent{
		Name:  "failed",
		Count: Failed,
	})

	Total := Success + Reschedule + Failed

	data = append(data, model.GroupEvent{
		Name:  "total",
		Count: Total,
	})

	return data, nil
}

func (uc *usecase) Graph(ctx context.Context) ([]map[string]interface{}, error) {

	result := make([]map[string]interface{}, 0)
	listCountSucces := make([]int, 0)
	listCountFailed := make([]int, 0)

	for i := 1; i <= 12; i++ {

		countSucces, _ := uc.eventDB.CountByStatus(ctx, "Success", i)
		listCountSucces = append(listCountSucces, countSucces)

		countFailed, _ := uc.eventDB.CountByStatus(ctx, "Cancel", i)
		listCountFailed = append(listCountFailed, countFailed)
	}

	result = append(result, map[string]interface{}{
		"status": "success",
		"count":  listCountSucces,
	})

	result = append(result, map[string]interface{}{
		"status": "failed",
		"count":  listCountFailed,
	})

	return result, nil
}

func (uc *usecase) DonutChart(ctx context.Context) (map[string]int, error) {

	result := make(map[string]int)

	countSucces, _ := uc.eventDB.CountByStatus(ctx, "Success", 0)
	countScheduled, _ := uc.eventDB.CountByStatus(ctx, "Scheduled", 0)

	result["success"] = countSucces
	result["scheduled"] = countScheduled

	return result, nil
}
