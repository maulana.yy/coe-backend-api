package usecase

import (
	"context"
	"errors"
	"mymodule/model"
)

func (uc *usecase) OrganizerAll(ctx context.Context, req model.GetAllReq) ([]model.Organizer, int, error) {

	return uc.organizerDB.Get(ctx, req)
}

func (uc *usecase) OrganizerGetCode(ctx context.Context) ([]model.DataMasterCodeOrganizerDic, error) {

	return uc.organizerDB.GetCode(ctx)
}

func (uc *usecase) OrganizerByID(ctx context.Context, ID int64) (model.Organizer, error) {

	return uc.organizerDB.GetDetail(ctx, ID)
}

func (uc *usecase) OrganizerCreate(ctx context.Context, body model.Organizer) error {

	organizer, _ := uc.organizerDB.GetByCode(ctx, body.Code)

	if organizer.Code != "" {
		return errors.New("Organizer Has Been Created")
	}

	return uc.organizerDB.Create(ctx, body)
}

func (uc *usecase) OrganizerUpdate(ctx context.Context, body model.Organizer) error {
	return uc.organizerDB.Update(ctx, body)
}

func (uc *usecase) OrganizerDelete(ctx context.Context, ID int64) error {
	return uc.organizerDB.Delete(ctx, ID)
}
