package constant

import "errors"

var (
	ErrDataNotFound = errors.New("data not found")
	FilterEmpty     = errors.New("filter date cannot empty")
)
