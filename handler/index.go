package handler

import (
	"context"
	"errors"
	"mymodule/app"
	"mymodule/constant"
	"mymodule/model"
	"mymodule/pkg"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/rs/zerolog"
)

type Handler struct {
	Usecase    app.Usecase
	Middleware *app.Middleware
	Log        *zerolog.Logger
	Config     *model.Config
}

func NewHandler(UC app.Usecase, cfg *model.Config, log *zerolog.Logger) *Handler {
	MW := app.NewMiddleware(UC, cfg, log)

	return &Handler{
		Usecase:    UC,
		Middleware: MW,
		Log:        log,
		Config:     cfg,
	}
}

func getGlobalContextFromEchoContext(c echo.Context) (context.Context, *pkg.GlobalVal, error) {
	globalC, ok := c.Get(constant.GlobalCtx).(pkg.GlobalVal)
	if !ok {
		return nil, nil, errors.New("global context not found")
	}
	userID, ok := c.Get(constant.UserIDCtx).(int64)
	if !ok {
		// TODO: add to log
	}

	globalC.SetUser(userID, "")

	ctx := c.Request().Context()
	ctx = pkg.AddExistingGlobalToContext(ctx, globalC)

	return ctx, &globalC, nil
}

func badRequest(c echo.Context, err error) error {
	var errCode int
	var errMsg string

	switch v := err.(type) {
	case *pkg.Error:
		errMsg = v.Err.Error()
		errCode = v.GetCode()

	default:
		errMsg = v.Error()
	}

	return c.JSON(http.StatusBadRequest, model.ResponseJSON{
		Success: false,
		Message: errMsg,
		Code:    errCode,
	})
}

func okResponse(c echo.Context, msg string, data interface{}) error {
	return c.JSON(http.StatusOK, model.ResponseJSON{
		Success: true,
		Message: msg,
		Data:    data,
	})
}
