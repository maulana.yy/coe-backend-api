package handler

import (
	"fmt"
	"io"
	"mymodule/model"
	"os"
	"strconv"
	"strings"

	"github.com/labstack/echo/v4"
)

func (h *Handler) CreateEvent(c echo.Context) error {
	var (
		body model.EventRequest
		path string
	)
	// var participantEvent []model.ParticipantEvent

	name := c.FormValue("name")
	category := c.FormValue("category")
	organizer := c.FormValue("organizer")
	room := c.FormValue("room")
	link := c.FormValue("link")
	department := c.FormValue("department")
	jabatan := c.FormValue("jabatan")
	startDate := c.FormValue("start_date")
	endDate := c.FormValue("end_date")
	startTime := c.FormValue("start_time")
	endTime := c.FormValue("end_time")
	participant := c.FormValue("participant")
	note := c.FormValue("note")
	support := c.FormValue("support")
	attachment, err := c.FormFile("attachment")

	if attachment != nil {
		if err != nil {
			fmt.Println(err)
			fmt.Println("ERROR GET FORM FILE")
			return badRequest(c, err)
		}
		src, err := attachment.Open()
		if err != nil {
			fmt.Println("ERROR READ FILE")
			return badRequest(c, err)
		}
		defer src.Close()
		// Destination
		path = "public/attachment/" + attachment.Filename
		dst, err := os.Create(path)
		if err != nil {
			return badRequest(c, err)
		}
		defer dst.Close()

		if _, err = io.Copy(dst, src); err != nil {
			return badRequest(c, err)
		}
	}

	ctx, globalV, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	userID := globalV.GetUserID()

	user, err := h.Usecase.UserGetByID(ctx, userID)
	if err != nil {
		return badRequest(c, err)
	}

	body = model.EventRequest{
		Name:        name,
		Category:    category,
		Organizer:   organizer,
		Room:        room,
		Link:        link,
		Level:       strings.Split(jabatan, ","),
		Department:  strings.Split(department, ","),
		Pic:         user.Username,
		StartDate:   startDate,
		EndDate:     endDate,
		StartTime:   startTime,
		EndTime:     endTime,
		Support:     strings.Split(support, ","),
		Participant: strings.Split(participant, ","),
		Note:        note,
		Attachment:  path,
		Createdby:   user.Username,
	}

	fmt.Println("BODY")
	fmt.Println(body)

	err = h.Usecase.EventCreate(ctx, body)
	if err != nil {
		return badRequest(c, err)
	}

	return okResponse(c, "Create Event Success", nil)
}

func (h *Handler) UpdateEvent(c echo.Context) error {

	var (
		body model.EventRequest
		path string
	)

	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		return badRequest(c, err)
	}

	// var participantEvent []model.ParticipantEvent

	name := c.FormValue("name")
	category := c.FormValue("category")
	organizer := c.FormValue("organizer")
	room := c.FormValue("room")
	link := c.FormValue("link")
	department := c.FormValue("department")
	jabatan := c.FormValue("jabatan")
	startDate := c.FormValue("start_date")
	endDate := c.FormValue("end_date")
	startTime := c.FormValue("start_time")
	endTime := c.FormValue("end_time")
	pic := c.FormValue("pic")
	participant := c.FormValue("participant")
	note := c.FormValue("note")
	support := c.FormValue("support")
	attachment, err := c.FormFile("attachment")

	if attachment != nil {
		if err != nil {
			fmt.Println(err)
			fmt.Println("ERROR GET FORM FILE")
			return badRequest(c, err)
		}
		src, err := attachment.Open()
		if err != nil {
			fmt.Println("ERROR READ FILE")
			return badRequest(c, err)
		}
		defer src.Close()
		// Destination
		path = "public/attachment/" + attachment.Filename
		dst, err := os.Create(path)
		if err != nil {
			return badRequest(c, err)
		}
		defer dst.Close()

		if _, err = io.Copy(dst, src); err != nil {
			return badRequest(c, err)
		}
	}

	ctx, globalV, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	userID := globalV.GetUserID()

	user, err := h.Usecase.UserGetByID(ctx, userID)
	if err != nil {
		return badRequest(c, err)
	}

	fmt.Println(user)

	body = model.EventRequest{
		Name:        name,
		Category:    category,
		Organizer:   organizer,
		Room:        room,
		Link:        link,
		Department:  strings.Split(department, ","),
		Level:       strings.Split(jabatan, ","),
		Pic:         pic,
		StartDate:   startDate,
		EndDate:     endDate,
		StartTime:   startTime,
		EndTime:     endTime,
		Support:     strings.Split(support, ","),
		Participant: strings.Split(participant, ","),
		Note:        note,
		Attachment:  path,
		Createdby:   user.Username,
	}

	err = h.Usecase.EventUpdate(ctx, body, id)
	if err != nil {
		return badRequest(c, err)
	}

	return okResponse(c, "Update Event Success", nil)
}

func (h *Handler) DeleteEvent(c echo.Context) error {

	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		return badRequest(c, err)
	}

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	err = h.Usecase.EventDelete(ctx, id)
	if err != nil {
		return badRequest(c, err)
	}

	return okResponse(c, "Delete Event Success", nil)
}

func (h *Handler) GetAllEvent(c echo.Context) error {

	search := c.QueryParam("search")
	limit, err := strconv.Atoi(c.QueryParam("limit"))
	offset, err := strconv.Atoi(c.QueryParam("offset"))
	startDate := c.QueryParam("start_date")
	endDate := c.QueryParam("end_date")

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	body := model.GetAllReq{
		Search:    search,
		Limit:     limit,
		Offset:    offset,
		StartDate: startDate,
		EndDate:   endDate,
	}

	data, count, err := h.Usecase.EventAll(ctx, body)
	if err != nil {
		fmt.Println(err)
		return badRequest(c, err)
	}

	result := map[string]interface{}{
		"data":  data,
		"total": count,
	}

	return okResponse(c, "Get Event Success", result)
}

func (h *Handler) GetDetailEvent(c echo.Context) error {
	fmt.Println("GET DETAIL")
	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		return badRequest(c, err)
	}

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	user, err := h.Usecase.EventByID(ctx, id)
	if err != nil {
		return badRequest(c, err)
	}

	return okResponse(c, "Get Event Success", user)
}

func (h *Handler) GetReport(c echo.Context) error {
	fmt.Println("GET REPORT")
	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		fmt.Println("BAD REQUEST")
		return badRequest(c, err)
	}

	filename, err := h.Usecase.EventGetReport(ctx)
	if err != nil {
		fmt.Println("FILE NAME ERROR")
		return badRequest(c, err)
	}

	return c.Attachment(filename, "book2.xlsx")
	// return okResponse(c, "Create Report Success", filename)
	// return c.File(filename)
}

func (h *Handler) ApproveEvent(c echo.Context) error {
	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		return badRequest(c, err)
	}
	status := c.QueryParam("status")
	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	err = h.Usecase.EventApprove(ctx, id, status)
	if err != nil {
		return badRequest(c, err)
	}
	return okResponse(c, "Get Event Success", nil)
}

func (h *Handler) UpdateStatusEvent(c echo.Context) error {
	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		return badRequest(c, err)
	}

	status := c.QueryParam("status")
	newDateStart := c.QueryParam("new_date_start")
	newDateEnd := c.QueryParam("new_date_end")
	newTimeStart := c.QueryParam("new_time_start")
	newTimeEnd := c.QueryParam("new_time_end")

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	err = h.Usecase.EventUpdateStatus(ctx, id, status, newDateStart, newDateEnd, newTimeStart, newTimeEnd)
	if err != nil {
		return badRequest(c, err)
	}
	return okResponse(c, "Get Event Success", nil)
}

func (h *Handler) GetToday(c echo.Context) error {
	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	event, err := h.Usecase.EventToday(ctx)
	if err != nil {
		return badRequest(c, err)
	}
	return okResponse(c, "Gept Event Success", event)
}

func (h *Handler) GetCalendarEvent(c echo.Context) error {
	calendar := c.QueryParam("calendar")
	startDate := c.QueryParam("startDate")
	endDate := c.QueryParam("endDate")

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	event, err := h.Usecase.EventOfCalendar(ctx, calendar, startDate, endDate)
	if err != nil {
		return badRequest(c, err)
	}
	return okResponse(c, "Get Event Success", event)
}
