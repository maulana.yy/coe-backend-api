package handler

import (
	"fmt"
	"io"
	"mymodule/model"
	"os"
	"strconv"

	"github.com/labstack/echo/v4"
)

func (h *Handler) CreateParticipant(c echo.Context) error {

	var body model.Participant
	if err := c.Bind(&body); err != nil {
		return badRequest(c, err)
	}

	ctx, globalV, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	userID := globalV.GetUserID()

	user, err := h.Usecase.UserGetByID(ctx, userID)
	if err != nil {
		return badRequest(c, err)
	}

	fmt.Println(body)
	body.CreatedBy = user.Username

	err = h.Usecase.ParticipantCreate(ctx, body)
	if err != nil {
		return badRequest(c, err)
	}

	return okResponse(c, "Create Participant Success", nil)
}

func (h *Handler) UpdateParticipant(c echo.Context) error {

	var body model.Participant

	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		return badRequest(c, err)
	}

	if err := c.Bind(&body); err != nil {
		return badRequest(c, err)
	}

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	body.ID = id

	err = h.Usecase.ParticipantUpdate(ctx, body)
	if err != nil {
		return badRequest(c, err)
	}

	return okResponse(c, "Update Participant Success", nil)
}

func (h *Handler) DeleteParticipant(c echo.Context) error {

	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		return badRequest(c, err)
	}

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}
	err = h.Usecase.ParticipantDelete(ctx, id)
	if err != nil {
		return badRequest(c, err)
	}

	return okResponse(c, "Delete Participant Success", nil)
}

func (h *Handler) GetAllParticipant(c echo.Context) error {

	limit, err := strconv.Atoi(c.QueryParam("limit"))
	offset, err := strconv.Atoi(c.QueryParam("offset"))
	startDate := c.QueryParam("start_date")
	endDate := c.QueryParam("end_date")
	search := c.QueryParam("search")

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	body := model.GetAllReq{
		Limit:     limit,
		Offset:    offset,
		StartDate: startDate,
		EndDate:   endDate,
		Search:    search,
	}

	data, count, err := h.Usecase.ParticipantGet(ctx, body)
	if err != nil {
		return badRequest(c, err)
	}

	result := map[string]interface{}{
		"data":  data,
		"total": count,
	}

	return okResponse(c, "Get Participant Success", result)
}

func (h *Handler) GetDetailParticipant(c echo.Context) error {

	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		return badRequest(c, err)
	}

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	user, err := h.Usecase.ParticipantGetDetail(ctx, id)
	if err != nil {
		return badRequest(c, err)
	}

	return okResponse(c, "Get Participant Success", user)
}

func (h *Handler) UplaodCSV(c echo.Context) error {
	var path string
	file, err := c.FormFile("file")
	if file != nil {
		if err != nil {
			fmt.Println(err)
			fmt.Println("ERROR GET FORM FILE")
			return badRequest(c, err)
		}
		src, err := file.Open()
		if err != nil {
			fmt.Println("ERROR READ FILE")
			return badRequest(c, err)
		}
		defer src.Close()
		// Destination
		path = "public/excel/" + file.Filename
		dst, err := os.Create(path)
		if err != nil {
			return badRequest(c, err)
		}
		defer dst.Close()

		if _, err = io.Copy(dst, src); err != nil {
			return badRequest(c, err)
		}
	}
	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	err = h.Usecase.InsertBulkCSV(ctx, file.Filename)
	if err != nil {
		return badRequest(c, err)
	}

	return okResponse(c, "Upload template Success", nil)
}

func (h *Handler) GetCodeParticipant(c echo.Context) error {

	organizer := c.QueryParam("organizer")
	level := c.QueryParam("level")

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	data, err := h.Usecase.ParticipantGetCode(ctx, organizer, level)
	if err != nil {
		return badRequest(c, err)
	}

	return okResponse(c, "Get Participant Success", data)
}
