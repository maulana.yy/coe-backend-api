package handler

import (
	"mymodule/model"
	"strconv"

	"github.com/labstack/echo/v4"
)

func (h *Handler) CreateHoliday(c echo.Context) error {
	var body model.HolidayReq
	if err := c.Bind(&body); err != nil {
		return badRequest(c, err)
	}

	ctx, globalV, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	userID := globalV.GetUserID()

	user, err := h.Usecase.UserGetByID(ctx, userID)
	if err != nil {
		return badRequest(c, err)
	}

	body.CreatedBy = user.Username

	err = h.Usecase.HolidayCreate(ctx, body)
	if err != nil {
		return badRequest(c, err)
	}

	return okResponse(c, "Create Holiday Success", nil)
}

func (h *Handler) UpdateHoliday(c echo.Context) error {

	var body model.HolidayReq

	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		return badRequest(c, err)
	}

	if err := c.Bind(&body); err != nil {
		return badRequest(c, err)
	}

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	body.ID = id
	err = h.Usecase.HolidayUpdate(ctx, body)
	if err != nil {
		return badRequest(c, err)
	}

	return okResponse(c, "Update Holiday Success", nil)
}

func (h *Handler) DeleteHoliday(c echo.Context) error {

	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		return badRequest(c, err)
	}

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	err = h.Usecase.HolidayDelete(ctx, id)
	if err != nil {
		return badRequest(c, err)
	}

	return okResponse(c, "Delete Holiday Success", nil)
}

func (h *Handler) GetAllHoliday(c echo.Context) error {

	search := c.QueryParam("search")
	limit, err := strconv.Atoi(c.QueryParam("limit"))
	offset, err := strconv.Atoi(c.QueryParam("offset"))
	startDate := c.QueryParam("start_date")
	endDate := c.QueryParam("end_date")

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	body := model.GetAllReq{
		Search:    search,
		Limit:     limit,
		Offset:    offset,
		StartDate: startDate,
		EndDate:   endDate,
	}

	data, count, err := h.Usecase.HolidayAll(ctx, body)
	if err != nil {
		return badRequest(c, err)
	}

	result := map[string]interface{}{
		"data":  data,
		"total": count,
	}

	return okResponse(c, "Get Holiday Success", result)
}

func (h *Handler) GetDetailHoliday(c echo.Context) error {

	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		return badRequest(c, err)
	}

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	user, err := h.Usecase.HolidayByID(ctx, id)
	if err != nil {
		return badRequest(c, err)
	}

	return okResponse(c, "Get Holiday Success", user)
}
