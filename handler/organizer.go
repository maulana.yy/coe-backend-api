package handler

import (
	"mymodule/model"
	"strconv"

	"github.com/labstack/echo/v4"
)

func (h *Handler) CreateOrganizer(c echo.Context) error {

	var body model.Organizer
	if err := c.Bind(&body); err != nil {
		return badRequest(c, err)
	}

	ctx, globalV, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	userID := globalV.GetUserID()

	user, err := h.Usecase.UserGetByID(ctx, userID)
	if err != nil {
		return badRequest(c, err)
	}

	body.CreatedBy = user.Username

	err = h.Usecase.OrganizerCreate(ctx, body)
	if err != nil {
		return badRequest(c, err)
	}

	return okResponse(c, "Create Organizer Success", nil)
}

func (h *Handler) UpdateOrganizer(c echo.Context) error {

	var body model.Organizer

	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		return badRequest(c, err)
	}

	if err := c.Bind(&body); err != nil {
		return badRequest(c, err)
	}

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	body.ID = id

	err = h.Usecase.OrganizerUpdate(ctx, body)
	if err != nil {
		return badRequest(c, err)
	}

	return okResponse(c, "Update Organizer Success", nil)
}

func (h *Handler) DeleteOrganizer(c echo.Context) error {

	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		return badRequest(c, err)
	}

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}
	err = h.Usecase.OrganizerDelete(ctx, id)
	if err != nil {
		return badRequest(c, err)
	}

	return okResponse(c, "Delete Organizer Success", nil)
}

func (h *Handler) GetAllOrganizer(c echo.Context) error {

	limit, err := strconv.Atoi(c.QueryParam("limit"))
	offset, err := strconv.Atoi(c.QueryParam("offset"))
	startDate := c.QueryParam("start_date")
	endDate := c.QueryParam("end_date")
	search := c.QueryParam("search")

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	body := model.GetAllReq{
		Limit:     limit,
		Offset:    offset,
		StartDate: startDate,
		EndDate:   endDate,
		Search:    search,
	}

	data, count, err := h.Usecase.OrganizerAll(ctx, body)
	if err != nil {
		return badRequest(c, err)
	}

	result := map[string]interface{}{
		"data":  data,
		"total": count,
	}

	return okResponse(c, "Get Organizer Success", result)
}

func (h *Handler) GetOrganizerCode(c echo.Context) error {

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	data, err := h.Usecase.OrganizerGetCode(ctx)
	if err != nil {
		return badRequest(c, err)
	}

	return okResponse(c, "Get Organizer Success", data)
}

func (h *Handler) GetDetailOrganizer(c echo.Context) error {

	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		return badRequest(c, err)
	}

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	user, err := h.Usecase.OrganizerByID(ctx, id)
	if err != nil {
		return badRequest(c, err)
	}

	return okResponse(c, "Get Organizer Success", user)
}
