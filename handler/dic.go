package handler

import (
	"mymodule/model"
	"strconv"

	"github.com/labstack/echo/v4"
)

func (h *Handler) CreateDic(c echo.Context) error {

	var body model.Dic
	if err := c.Bind(&body); err != nil {
		return badRequest(c, err)
	}

	ctx, globalV, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	userID := globalV.GetUserID()

	user, err := h.Usecase.UserGetByID(ctx, userID)
	if err != nil {
		return badRequest(c, err)
	}

	body.CreatedBy = user.Username

	err = h.Usecase.DicCreate(ctx, body)
	if err != nil {
		return badRequest(c, err)
	}

	return okResponse(c, "Create Dic Success", nil)
}

func (h *Handler) UpdateDic(c echo.Context) error {

	var body model.Dic

	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		return badRequest(c, err)
	}

	if err := c.Bind(&body); err != nil {
		return badRequest(c, err)
	}

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	body.ID = id

	err = h.Usecase.DicUpdate(ctx, body)
	if err != nil {
		return badRequest(c, err)
	}

	return okResponse(c, "Update Dic Success", nil)
}

func (h *Handler) DeleteDic(c echo.Context) error {

	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		return badRequest(c, err)
	}

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}
	err = h.Usecase.DicDelete(ctx, id)
	if err != nil {
		return badRequest(c, err)
	}

	return okResponse(c, "Delete Dic Success", nil)
}

func (h *Handler) GetAllDic(c echo.Context) error {

	limit, err := strconv.Atoi(c.QueryParam("limit"))
	offset, err := strconv.Atoi(c.QueryParam("offset"))
	startDate := c.QueryParam("start_date")
	endDate := c.QueryParam("end_date")
	search := c.QueryParam("search")

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	body := model.GetAllReq{
		Limit:     limit,
		Offset:    offset,
		StartDate: startDate,
		EndDate:   endDate,
		Search:    search,
	}

	data, count, err := h.Usecase.DicAll(ctx, body)
	if err != nil {
		return badRequest(c, err)
	}

	result := map[string]interface{}{
		"data":  data,
		"total": count,
	}

	return okResponse(c, "Get Dic Success", result)
}

func (h *Handler) GetDicCode(c echo.Context) error {

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	data, err := h.Usecase.DicGetCode(ctx)
	if err != nil {
		return badRequest(c, err)
	}

	return okResponse(c, "Get Dic Success", data)
}

func (h *Handler) GetDetailDic(c echo.Context) error {

	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		return badRequest(c, err)
	}

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	user, err := h.Usecase.DicByID(ctx, id)
	if err != nil {
		return badRequest(c, err)
	}

	return okResponse(c, "Get Dic Success", user)
}
