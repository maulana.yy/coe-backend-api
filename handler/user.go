package handler

import (
	"fmt"
	"io"
	"mymodule/model"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/labstack/echo/v4"
)

func (h *Handler) Login(c echo.Context) error {

	var reqData model.Login
	if err := c.Bind(&reqData); err != nil {
		return badRequest(c, err)
	}

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		fmt.Println(err)
		return badRequest(c, err)
	}

	data, err := h.Usecase.Login(ctx, reqData)
	if err != nil {
		fmt.Println(err)
		return badRequest(c, err)
	}

	return okResponse(c, "login success", data)
}

func (h *Handler) Logout(c echo.Context) error {

	return c.JSON(http.StatusOK, model.ResponseJSON{
		Success: true,
		Message: "Logout Success",
		Data:    nil,
	})
}

func (h *Handler) CreateUser(c echo.Context) error {
	var (
		body model.User
		path string
	)

	username := c.FormValue("username")
	password := c.FormValue("password")
	name := c.FormValue("name")
	email := c.FormValue("email")
	role := c.FormValue("role")
	department := c.FormValue("department")
	level := c.FormValue("level")
	file, err := c.FormFile("profile")

	if file != nil {
		fmt.Println("FILE : ", file)
		if err != nil {
			fmt.Println(err)
			fmt.Println("ERROR GET FORM FILE")
			return badRequest(c, err)
		}
		src, err := file.Open()
		if err != nil {
			fmt.Println("ERROR READ FILE")
			return badRequest(c, err)
		}
		defer src.Close()
		// Destination
		path = "public/img/" + file.Filename
		dst, err := os.Create(path)
		if err != nil {
			return badRequest(c, err)
		}
		defer dst.Close()

		if _, err = io.Copy(dst, src); err != nil {
			return badRequest(c, err)
		}
	}

	ctx, globalV, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	userID := globalV.GetUserID()

	user, err := h.Usecase.UserGetByID(ctx, userID)
	if err != nil {
		return badRequest(c, err)
	}

	body = model.User{
		Username: username,
		Password: password,
		Email:    email,
		Info: model.Info{
			Name:       name,
			Profile:    path,
			Role:       role,
			Department: department,
			Level:      level,
		},
		CreatedBy: user.Username,
	}

	err = h.Usecase.UserCreate(ctx, body)
	if err != nil {
		return badRequest(c, err)
	}

	return okResponse(c, "Create User Success", nil)
}

func (h *Handler) UpdateUser(c echo.Context) error {

	var (
		body model.UserUpdate
		path string
	)

	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		return badRequest(c, err)
	}

	name := c.FormValue("name")
	email := c.FormValue("email")
	role := c.FormValue("role")
	department := c.FormValue("department")
	level := c.FormValue("level")
	file, err := c.FormFile("profile")

	if file != nil {
		fmt.Println("FILE : ", file)
		if err != nil {
			fmt.Println(err)
			fmt.Println("ERROR GET FORM FILE")
			return badRequest(c, err)
		}
		src, err := file.Open()
		if err != nil {
			fmt.Println("ERROR READ FILE")
			return badRequest(c, err)
		}
		defer src.Close()
		// Destination
		path = "public/img/" + file.Filename
		dst, err := os.Create(path)
		if err != nil {
			return badRequest(c, err)
		}
		defer dst.Close()

		if _, err = io.Copy(dst, src); err != nil {
			return badRequest(c, err)
		}
	}

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	body = model.UserUpdate{
		ID:         id,
		Email:      email,
		Name:       name,
		Profile:    path,
		Role:       role,
		Department: department,
		Level:      level,
	}

	err = h.Usecase.UserUpdate(ctx, body)
	if err != nil {
		return badRequest(c, err)
	}

	return okResponse(c, "Update User Success", nil)
}

func (h *Handler) DeleteUser(c echo.Context) error {
	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		return badRequest(c, err)
	}

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	err = h.Usecase.UserDelete(ctx, id)
	if err != nil {
		return badRequest(c, err)
	}

	return okResponse(c, "Delete User Success", nil)
}

func (h *Handler) GetAllUser(c echo.Context) error {

	search := c.QueryParam("search")
	limit, err := strconv.Atoi(c.QueryParam("limit"))
	offset, err := strconv.Atoi(c.QueryParam("offset"))
	startDate := c.QueryParam("start_date")
	endDate := c.QueryParam("end_date")

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	body := model.GetAllReq{
		Search:    search,
		Limit:     limit,
		Offset:    offset,
		StartDate: startDate,
		EndDate:   endDate,
	}

	data, count, err := h.Usecase.UserAll(ctx, body)
	if err != nil {
		return badRequest(c, err)
	}

	result := map[string]interface{}{
		"data":  data,
		"total": count,
	}

	return okResponse(c, "Get User Success", result)
}

func (h *Handler) GetUserCode(c echo.Context) error {

	department := c.QueryParam("department")

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	data, err := h.Usecase.UserGetCode(ctx, strings.Split(department, ","))
	if err != nil {
		return badRequest(c, err)
	}

	return okResponse(c, "Get User Success", data)
}

func (h *Handler) GetDetailUser(c echo.Context) error {

	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		return badRequest(c, err)
	}

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	user, err := h.Usecase.UserGetByID(ctx, id)
	if err != nil {
		return badRequest(c, err)
	}

	return okResponse(c, "Get User Success", user)
}

func (h *Handler) GetFileProfile(c echo.Context) error {

	var filePath string
	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		return badRequest(c, err)
	}

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	data, err := h.Usecase.UserGetByID(ctx, id)
	if err != nil {
		return badRequest(c, err)
	}

	if data.Info.Profile == "" {
		filePath = "public/placeholder/200x200.jpg"
	} else {
		filePath = data.Info.Profile
	}

	f, err := os.Open(filePath)
	if err != nil {
		return err
	}

	return c.Stream(http.StatusOK, "image/png", f)
}