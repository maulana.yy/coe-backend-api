package handler

import (
	"mymodule/model"
	"strconv"

	"github.com/labstack/echo/v4"
)

func (h *Handler) CreateInformation(c echo.Context) error {
	var body model.Information
	if err := c.Bind(&body); err != nil {
		return badRequest(c, err)
	}

	ctx, globalV, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	userID := globalV.GetUserID()

	user, err := h.Usecase.UserGetByID(ctx, userID)
	if err != nil {
		return badRequest(c, err)
	}

	body.CreatedBy = user.Username

	err = h.Usecase.InformationCreate(ctx, body)
	if err != nil {
		return badRequest(c, err)
	}

	return okResponse(c, "Create Information Success", nil)
}

func (h *Handler) UpdateInformation(c echo.Context) error {

	var body model.Information

	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		return badRequest(c, err)
	}

	if err := c.Bind(&body); err != nil {
		return badRequest(c, err)
	}

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	body.ID = id
	err = h.Usecase.InformationUpdate(ctx, body)
	if err != nil {
		return badRequest(c, err)
	}

	return okResponse(c, "Update Information Success", nil)
}

func (h *Handler) DeleteInformation(c echo.Context) error {

	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		return badRequest(c, err)
	}

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	err = h.Usecase.InformationDelete(ctx, id)
	if err != nil {
		return badRequest(c, err)
	}

	return okResponse(c, "Delete Information Success", nil)
}

func (h *Handler) GetAllInformation(c echo.Context) error {

	search := c.QueryParam("search")
	limit, err := strconv.Atoi(c.QueryParam("limit"))
	offset, err := strconv.Atoi(c.QueryParam("offset"))
	startDate := c.QueryParam("start_date")
	endDate := c.QueryParam("end_date")

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	body := model.GetAllReq{
		Search:    search,
		Limit:     limit,
		Offset:    offset,
		StartDate: startDate,
		EndDate:   endDate,
	}

	data, count, err := h.Usecase.InformationAll(ctx, body)
	if err != nil {
		return badRequest(c, err)
	}

	result := map[string]interface{}{
		"data":  data,
		"total": count,
	}

	return okResponse(c, "Get Information Success", result)
}

func (h *Handler) GetDetailInformation(c echo.Context) error {

	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		return badRequest(c, err)
	}

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	user, err := h.Usecase.InformationByID(ctx, id)
	if err != nil {
		return badRequest(c, err)
	}

	return okResponse(c, "Get Information Success", user)
}

func (h *Handler) ChangeStatus(c echo.Context) error {
	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		return badRequest(c, err)
	}
	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	err = h.Usecase.InformationChangeStatus(ctx, id)
	if err != nil {
		return badRequest(c, err)
	}
	return okResponse(c, "Get Event Success", nil)
}
