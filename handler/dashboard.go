package handler

import (
	"fmt"
	"mymodule/model"
	"net/http"

	"github.com/labstack/echo/v4"
)

func (h *Handler) GetInfo(c echo.Context) error {

	fmt.Println("GET INFO DASHBOARD")
	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	data, err := h.Usecase.GetInfo(ctx)
	if err != nil {
		return badRequest(c, err)
	}
	return c.JSON(http.StatusOK, model.ResponseJSON{
		Success: true,
		Message: "Get Info Success",
		Data:    data,
	})
}

func (h *Handler) GetEvent(c echo.Context) error {

	fmt.Println("GET EVENT DASHBOARD")
	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	data, err := h.Usecase.GetEventToday(ctx)
	if err != nil {
		return badRequest(c, err)
	}
	return c.JSON(http.StatusOK, model.ResponseJSON{
		Success: true,
		Message: "Get Event Success",
		Data:    data,
	})
}

func (h *Handler) GetCalendar(c echo.Context) error {

	fmt.Println("GET HOME CALENDAR")
	startDate := c.QueryParam("startDate")
	endDate := c.QueryParam("endDate")
	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	event, err := h.Usecase.EventOfCalendar(ctx, "month", startDate, endDate)
	if err != nil {
		fmt.Println("ERROR")
		return badRequest(c, err)
	}
	return okResponse(c, "Get Calendar Success", event)
}

func (h *Handler) GetGraph(c echo.Context) error {

	fmt.Println("GET GROUP GRAPH")
	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	data, err := h.Usecase.Graph(ctx)
	if err != nil {
		return badRequest(c, err)
	}

	return c.JSON(http.StatusOK, model.ResponseJSON{
		Success: true,
		Message: "Get Graph Success",
		Data:    data,
	})
}

func (h *Handler) GetGroupEvent(c echo.Context) error {

	fmt.Println("GET GROUP EVENT DASHBOARD")
	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	data, err := h.Usecase.GroupingEvent(ctx)
	if err != nil {
		return badRequest(c, err)
	}

	return c.JSON(http.StatusOK, model.ResponseJSON{
		Success: true,
		Message: "Get Group Event",
		Data:    data,
	})
}

func (h *Handler) GetDonutChart(c echo.Context) error {

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	data, err := h.Usecase.DonutChart(ctx)
	if err != nil {
		return badRequest(c, err)
	}

	return c.JSON(http.StatusOK, model.ResponseJSON{
		Success: true,
		Message: "Get Donut Chart Success",
		Data:    data,
	})
}
