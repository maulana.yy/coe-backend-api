package handler

import (
	"mymodule/model"
	"strconv"

	"github.com/labstack/echo/v4"
)

func (h *Handler) CreateRole(c echo.Context) error {
	var body model.Role
	if err := c.Bind(&body); err != nil {
		return badRequest(c, err)
	}

	ctx, globalV, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	userID := globalV.GetUserID()

	user, err := h.Usecase.UserGetByID(ctx, userID)
	if err != nil {
		return badRequest(c, err)
	}

	body.CreatedBy = user.Username

	err = h.Usecase.RoleCreate(ctx, body)
	if err != nil {
		return badRequest(c, err)
	}

	return okResponse(c, "Create Role Success", nil)
}

func (h *Handler) UpdateRole(c echo.Context) error {

	var body model.Role

	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		return badRequest(c, err)
	}

	if err := c.Bind(&body); err != nil {
		return badRequest(c, err)
	}

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	body.ID = id
	err = h.Usecase.RoleUpdate(ctx, body)
	if err != nil {
		return badRequest(c, err)
	}

	return okResponse(c, "Update Role Success", nil)
}

func (h *Handler) DeleteRole(c echo.Context) error {

	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		return badRequest(c, err)
	}

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	err = h.Usecase.RoleDelete(ctx, id)
	if err != nil {
		return badRequest(c, err)
	}

	return okResponse(c, "Delete Role Success", nil)
}

func (h *Handler) GetAllRole(c echo.Context) error {

	search := c.QueryParam("search")
	limit, err := strconv.Atoi(c.QueryParam("limit"))
	offset, err := strconv.Atoi(c.QueryParam("offset"))
	startDate := c.QueryParam("start_date")
	endDate := c.QueryParam("end_date")

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	body := model.GetAllReq{
		Search:    search,
		Limit:     limit,
		Offset:    offset,
		StartDate: startDate,
		EndDate:   endDate,
	}

	data, count, err := h.Usecase.RoleAll(ctx, body)
	if err != nil {
		return badRequest(c, err)
	}

	result := map[string]interface{}{
		"data":  data,
		"total": count,
	}

	return okResponse(c, "Get Role Success", result)
}

func (h *Handler) GetRoleCode(c echo.Context) error {

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	data, err := h.Usecase.RoleGetCode(ctx)
	if err != nil {
		return badRequest(c, err)
	}

	return okResponse(c, "Get Role Success", data)
}

func (h *Handler) GetDetailRole(c echo.Context) error {

	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		return badRequest(c, err)
	}

	ctx, _, err := getGlobalContextFromEchoContext(c)
	if err != nil {
		return badRequest(c, err)
	}

	user, err := h.Usecase.RoleByID(ctx, id)
	if err != nil {
		return badRequest(c, err)
	}

	return okResponse(c, "Get Role Success", user)
}
