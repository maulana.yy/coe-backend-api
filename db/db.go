package db

import (
	"context"
	"mymodule/model"
)

type User interface {
	Create(ctx context.Context, body *model.User) error
	Update(ctx context.Context, body model.UserUpdate) error
	Delete(ctx context.Context, ID int64) error
	Get(ctx context.Context, body model.GetAllReq) ([]model.User, int, error)
	GetByUsername(ctx context.Context, username string) (model.User, error)
	GetDetail(ctx context.Context, ID int64) (*model.User, error)
	GetCode(ctx context.Context, department []string) ([]model.UserCode, error)
}

type Category interface {
	Create(ctx context.Context, body model.Category) error
	Update(ctx context.Context, body model.Category) error
	Delete(ctx context.Context, ID int64) error
	Get(ctx context.Context, body model.GetAllReq) ([]model.Category, int, error)
	GetDetail(ctx context.Context, ID int64) (model.Category, error)
	GetByName(ctx context.Context, name string) (model.Category, error)
	GetCode(ctx context.Context) ([]model.DataMasterCode, error)
}

type Organizer interface {
	Create(ctx context.Context, body model.Organizer) error
	Update(ctx context.Context, body model.Organizer) error
	Delete(ctx context.Context, ID int64) error
	Get(ctx context.Context, body model.GetAllReq) ([]model.Organizer, int, error)
	GetDetail(ctx context.Context, ID int64) (model.Organizer, error)
	GetByCode(ctx context.Context, code string) (model.Organizer, error)
	GetCode(ctx context.Context) ([]model.DataMasterCodeOrganizerDic, error)
}

type Role interface {
	Create(ctx context.Context, body model.Role) error
	Update(ctx context.Context, body model.Role) error
	Delete(ctx context.Context, ID int64) error
	Get(ctx context.Context, body model.GetAllReq) ([]model.Role, int, error)
	GetDetail(ctx context.Context, ID int64) (model.Role, error)
	GetCode(ctx context.Context) ([]model.DataMasterCode, error)
	GetByName(ctx context.Context, name string) (model.Role, error)
}

type Event interface {
	Create(ctx context.Context, body model.Event) error
	Update(ctx context.Context, body model.Event) error
	Delete(ctx context.Context, ID int64) error
	Get(ctx context.Context, body model.GetAllReq) ([]model.EventInfo, int, error)
	GetDetail(ctx context.Context, ID int64) (model.Event, error)
	UpdateApproval(ctx context.Context, ID int64, approval string) error
	UpdateStatus(ctx context.Context, ID int64, status, newDateStart, newDateEnd, newTimeStart, newTimeEnd string) error
	CountByStatus(ctx context.Context, Status string, Month int) (int, error)
	Today(ctx context.Context) ([]model.Event, error)
	GetCalendar(ctx context.Context, calendar, startDate, endDate string, index int) ([]model.EventInfo, error)
}

type Support interface {
	Create(ctx context.Context, body model.Support) error
	Update(ctx context.Context, body model.Support) error
	Delete(ctx context.Context, ID int64) error
	Get(ctx context.Context, body model.GetAllReq) ([]model.Support, int, error)
	GetDetail(ctx context.Context, ID int64) (model.Support, error)
	GetCode(ctx context.Context) ([]model.DataMasterCode, error)
}

type Holiday interface {
	Create(ctx context.Context, body model.Holiday) error
	Update(ctx context.Context, body model.Holiday) error
	Delete(ctx context.Context, ID int64) error
	Get(ctx context.Context, body model.GetAllReq) ([]model.Holiday, int, error)
	GetDetail(ctx context.Context, ID int64) (model.Holiday, error)
	GetCode(ctx context.Context) ([]model.DataMasterCode, error)
	GetCalendar(ctx context.Context, calendar, startDate, endDate string, index int) ([]model.Holiday, error)
}

type Information interface {
	Create(ctx context.Context, body model.Information) error
	Update(ctx context.Context, body model.Information) error
	Delete(ctx context.Context, ID int64) error
	Get(ctx context.Context, body model.GetAllReq) ([]model.Information, int, error)
	GetDetail(ctx context.Context, ID int64) (model.Information, error)
	ChangeStatus(ctx context.Context, ID int64) error
	GetInfoDashboard(ctx context.Context) ([]model.Information, int, error)
}

type Dic interface {
	Create(ctx context.Context, body model.Dic) error
	Update(ctx context.Context, body model.Dic) error
	Delete(ctx context.Context, ID int64) error
	Get(ctx context.Context, body model.GetAllReq) ([]model.Dic, int, error)
	GetDetail(ctx context.Context, ID int64) (model.Dic, error)
	GetCode(ctx context.Context) ([]model.DataMasterCodeOrganizerDic, error)
}

type Participant interface {
	Create(ctx context.Context, body *model.Participant) error
	Update(ctx context.Context, body model.Participant) error
	Delete(ctx context.Context, ID int64) error
	Get(ctx context.Context, body model.GetAllReq) ([]model.Participant, int, error)
	GetDetail(ctx context.Context, ID int64) (*model.Participant, error)
	GetDByEmail(ctx context.Context, email string) (*model.Participant, error)
	GetCode(ctx context.Context, organizer, level string) ([]model.ParticipantCode, error)
}
