package main

import (
	"context"
	"fmt"
	"mymodule/app"
	"mymodule/handler"
	"mymodule/model"
	"mymodule/pkg"
	"mymodule/route"
	"mymodule/usecase"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	log "github.com/rs/zerolog/log"
	"github.com/tylerb/graceful"
)

func main() {

	filePath := "./config.yaml"

	var cfg model.Config
	err := pkg.ReadFile(&cfg, filePath)
	if err != nil {
		log.Fatal().Err(err)
	}

	db, err := pkg.NewDatabase(context.Background(), model.DBParams{
		Address:  cfg.DB.Address,
		DBName:   cfg.DB.Name,
		Port:     cfg.DB.Port,
		SSLMode:  cfg.DB.SSL,
		User:     cfg.DB.User,
		Password: cfg.DB.Password,
	})
	if err != nil {
		log.Fatal().Err(err)
	}

	zLog, err := app.InitLogger()
	if err != nil {
		log.Fatal().Err(err)
	}

	repoDB := app.NewRepo(db)
	coeUC := usecase.NewUsecase(repoDB, &cfg)
	handler := handler.NewHandler(coeUC, &cfg, zLog)

	e := echo.New()

	route.RegisterRoute(e, handler)
	e.Use(middleware.CORSWithConfig(middleware.DefaultCORSConfig))
	e.Use(middleware.BodyLimit(cfg.File.MaxSize))
	e.Use(middleware.Logger())

	e.Server.Addr = ":" + cfg.Rest.Port
	fmt.Println("SERVER RUNNING IN PORT  : ", cfg.Rest.Port)
	graceful.ListenAndServe(e.Server, 10*time.Second)

}
