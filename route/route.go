package route

import (
	"mymodule/handler"
	"net/http"

	"github.com/labstack/echo/v4"
)

func RegisterRoute(e *echo.Echo, h *handler.Handler) {
	e.Use(h.Middleware.MiddlewareLogging, h.Middleware.Recover)

	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!")
	})

	v1 := e.Group("/v1")

	v1.POST("/login", h.Login)
	v1.POST("/logout", h.Logout)
	v1.GET("/user/:id/file", h.GetFileProfile)

	user := v1.Group("/user")
	user.Use(h.Middleware.MiddlewareToken)

	user.GET("", h.GetAllUser)
	user.GET("/:id/detail", h.GetDetailUser)
	user.GET("/code", h.GetUserCode)
	user.POST("", h.CreateUser)
	user.PUT("/:id/update", h.UpdateUser)
	user.DELETE("/:id/delete", h.DeleteUser)

	category := v1.Group("/category")
	category.Use(h.Middleware.MiddlewareToken)
	category.GET("", h.GetAllCategory)
	category.GET("/:id/detail", h.GetDetailCategory)
	category.GET("/code", h.GetCategoryCode)
	category.POST("", h.CreateCategory)
	category.PUT("/:id/update", h.UpdateCategory)
	category.DELETE("/:id/delete", h.DeleteCategory)

	organizer := v1.Group("/organizer")
	organizer.Use(h.Middleware.MiddlewareToken)
	organizer.GET("", h.GetAllOrganizer)
	organizer.GET("/:id/detail", h.GetDetailOrganizer)
	organizer.GET("/code", h.GetOrganizerCode)
	organizer.POST("", h.CreateOrganizer)
	organizer.PUT("/:id/update", h.UpdateOrganizer)
	organizer.DELETE("/:id/delete", h.DeleteOrganizer)

	// dic := v1.Group("/dic")
	// dic.Use(h.Middleware.MiddlewareToken)
	// dic.GET("", h.GetAllDic)
	// dic.GET("/:id/detail", h.GetDetailDic)
	// dic.GET("/code", h.GetDicCode)
	// dic.POST("", h.CreateDic)
	// dic.PUT("/:id/update", h.UpdateDic)
	// dic.DELETE("/:id/delete", h.DeleteDic)

	role := v1.Group("/role")
	role.Use(h.Middleware.MiddlewareToken)
	role.GET("", h.GetAllRole)
	role.GET("/:id/detail", h.GetDetailRole)
	role.GET("/code", h.GetRoleCode)
	role.POST("", h.CreateRole)
	role.PUT("/:id/update", h.UpdateRole)
	role.DELETE("/:id/delete", h.DeleteRole)

	event := v1.Group("/event")
	event.Use(h.Middleware.MiddlewareToken)
	event.GET("", h.GetAllEvent)
	event.GET("/:id/detail", h.GetDetailEvent)
	event.POST("", h.CreateEvent)
	event.PUT("/:id/update", h.UpdateEvent)
	event.DELETE("/:id/delete", h.DeleteEvent)
	event.GET("/report", h.GetReport)
	event.GET("/:id/approve", h.ApproveEvent)
	event.GET("/:id/status", h.UpdateStatusEvent)
	event.GET("/today", h.GetToday)
	event.GET("/calendar", h.GetCalendarEvent)

	support := v1.Group("/support")
	support.Use(h.Middleware.MiddlewareToken)
	support.GET("", h.GetAllSupport)
	support.GET("/:id/detail", h.GetDetailSupport)
	support.GET("/code", h.GetSupportCode)
	support.POST("", h.CreateSupport)
	support.PUT("/:id/update", h.UpdateSupport)
	support.DELETE("/:id/delete", h.DeleteSupport)

	holiday := v1.Group("/holiday")
	holiday.Use(h.Middleware.MiddlewareToken)
	holiday.GET("", h.GetAllHoliday)
	holiday.GET("/:id/detail", h.GetDetailHoliday)
	holiday.POST("", h.CreateHoliday)
	holiday.PUT("/:id/update", h.UpdateHoliday)
	holiday.DELETE("/:id/delete", h.DeleteHoliday)

	information := v1.Group("/information")
	information.Use(h.Middleware.MiddlewareToken)
	information.GET("", h.GetAllInformation)
	information.GET("/:id/detail", h.GetDetailInformation)
	information.GET("/:id/status", h.ChangeStatus)
	information.POST("", h.CreateInformation)
	information.PUT("/:id/update", h.UpdateInformation)
	information.DELETE("/:id/delete", h.DeleteInformation)

	participant := v1.Group("/participant")
	participant.Use(h.Middleware.MiddlewareToken)
	participant.GET("", h.GetAllParticipant)
	participant.GET("/code",h.GetCodeParticipant)
	participant.GET("/:id/detail", h.GetDetailParticipant)
	participant.POST("", h.CreateParticipant)
	participant.POST("/csv", h.UplaodCSV)
	participant.PUT("/:id/update", h.UpdateParticipant)
	participant.DELETE("/:id/delete", h.DeleteParticipant)

	dashboard := v1.Group("/dashboard")
	dashboard.GET("/info", h.GetInfo)
	dashboard.GET("/event", h.GetEvent)
	dashboard.GET("/calendar", h.GetCalendar)
	dashboard.GET("/graph", h.GetGraph)
	dashboard.GET("/group-event", h.GetGroupEvent)
	dashboard.GET("/donut", h.GetDonutChart)
}
