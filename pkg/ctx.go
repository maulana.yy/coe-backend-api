package pkg

import (
	"context"
	"mymodule/constant"
	"time"

	"github.com/google/uuid"
)

type GlobalVal struct {
	requestID   string // This is an UUID
	requestTime time.Time
	userID      int64
	username    string
}

func (c *GlobalVal) GetRequestID() string {
	return c.requestID
}

func (c *GlobalVal) GetRequestTime() time.Time {
	return c.requestTime
}

func (c *GlobalVal) GetUserID() int64 {
	return c.userID
}

func (c *GlobalVal) GetUsername() string {
	return c.username
}

func (c *GlobalVal) SetUser(id int64, username string) {
	c.username = username
	c.userID = id
}

// Use this to generate GlobalVal
func NewGlobalValue() GlobalVal {
	return GlobalVal{
		requestID:   uuid.New().String(),
		requestTime: time.Now(),
	}
}

// Use this to generate new context with GlobalContextValues
func NewGlobalContext() context.Context {
	return context.WithValue(context.Background(), constant.GlobalCtx, NewGlobalValue())
}

// Use this to add GlobalVal to existing context
func AddGlobalContext(ctx context.Context) context.Context {
	return context.WithValue(ctx, constant.GlobalCtx, NewGlobalValue())
}

// Use this to add existing GlobalVal to existing context
func AddExistingGlobalToContext(ctx context.Context, g GlobalVal) context.Context {
	return context.WithValue(ctx, constant.GlobalCtx, g)
}

// This function is for getting GlobalVal in a context
// will return pointer with time only
func GetGlobalValue(ctx context.Context) *GlobalVal {
	globalC, ok := ctx.Value(constant.GlobalCtx).(GlobalVal)
	if !ok {
		return nil
	}
	return &globalC
}
