package repo

import (
	"context"
	"fmt"
	"mymodule/db"
	"mymodule/model"
	"time"

	"github.com/go-pg/pg"
	"github.com/lib/pq"
)

type organizer struct {
	db *pg.DB
}

func NewOrganizer(db *pg.DB) db.Organizer {
	return &organizer{
		db: db,
	}
}

func (o *organizer) Create(ctx context.Context, body model.Organizer) error {

	db := o.db.Conn()
	defer db.Close()

	_, err := db.Model(&body).Insert()

	return err
}

func (o *organizer) Update(ctx context.Context, body model.Organizer) error {

	db := o.db.Conn()
	defer db.Close()

	var newData model.Organizer

	err := db.Model(&model.Organizer{
		ID: body.ID,
	}).WherePK().Select(&newData)

	newData.Name = body.Name
	newData.Code = body.Code
	newData.UpdatedAt = pq.NullTime{
		Time:  time.Now(),
		Valid: true,
	}

	_, err = db.Model(&newData).WherePK().Update()

	return err
}

func (o *organizer) Delete(ctx context.Context, ID int64) error {

	db := o.db.Conn()
	defer db.Close()

	data := &model.Organizer{
		ID: ID,
	}

	_, err := db.Model(data).WherePK().Delete()
	fmt.Println(err)

	return err
}

func (o *organizer) Get(ctx context.Context, body model.GetAllReq) ([]model.Organizer, int, error) {

	db := o.db.Conn()
	defer db.Close()
	var (
		data []model.Organizer
	)

	query := "deleted_at is null"

	count, err := db.Model(&model.Organizer{}).Where(query).Count()

	if err != nil {
		return data, 0, err
	}

	if body.Search != "" {
		query += " and LOWER(name) LIKE '%" + body.Search + "%' or LOWER(code) LIKE '%" + body.Search + "%'"
	}

	offset := (body.Offset - 1) * body.Limit

	err = db.Model(&model.Organizer{}).OrderExpr("id ASC").
		Where(query).
		Limit(body.Limit).
		Offset(offset).
		Select(&data)

	if err != nil {
		fmt.Println(err)
		return data, 0, err
	}

	return data, count, nil
}

func (o *organizer) GetDetail(ctx context.Context, ID int64) (model.Organizer, error) {

	db := o.db.Conn()
	defer db.Close()

	organizer := model.Organizer{
		ID: ID,
	}

	err := db.Model(&organizer).WherePK().First()
	if err != nil {
		return organizer, err
	}

	return organizer, nil
}

func (o *organizer) GetByCode(ctx context.Context, code string) (model.Organizer, error) {

	db := o.db.Conn()
	defer db.Close()

	organizer := model.Organizer{}

	query := "code = '" + code + "'"
	err := db.Model(&organizer).Where(query).First()
	if err != nil {
		return organizer, err
	}

	return organizer, nil
}

func (o *organizer) GetCode(ctx context.Context) ([]model.DataMasterCodeOrganizerDic, error) {

	data := make([]model.DataMasterCodeOrganizerDic, 0)

	db := o.db.Conn()
	defer db.Close()

	err := db.Model(&model.Organizer{}).Where("deleted_at is null").OrderExpr("id ASC").Column("id", "name", "code").Select(&data)

	if err != nil {
		return data, err
	}

	return data, nil
}
