package repo

import (
	"context"
	"fmt"
	"mymodule/db"
	"mymodule/model"
	"time"

	"github.com/go-pg/pg"
	"github.com/lib/pq"
)

type dic struct {
	db *pg.DB
}

func NewDic(db *pg.DB) db.Dic {
	return &dic{
		db: db,
	}
}

func (o *dic) Create(ctx context.Context, body model.Dic) error {

	db := o.db.Conn()
	defer db.Close()

	_, err := db.Model(&body).Insert()

	return err
}

func (o *dic) Update(ctx context.Context, body model.Dic) error {

	db := o.db.Conn()
	defer db.Close()

	var newData model.Dic

	err := db.Model(&model.Dic{
		ID: body.ID,
	}).WherePK().Select(&newData)

	newData.Name = body.Name
	newData.Code = body.Code
	newData.UpdatedAt = pq.NullTime{
		Time:  time.Now(),
		Valid: true,
	}

	_, err = db.Model(&newData).WherePK().Update()

	return err
}

func (o *dic) Delete(ctx context.Context, ID int64) error {

	db := o.db.Conn()
	defer db.Close()

	data := &model.Dic{
		ID: ID,
	}

	_, err := db.Model(data).WherePK().Delete()
	fmt.Println(err)

	return err
}

func (o *dic) Get(ctx context.Context, body model.GetAllReq) ([]model.Dic, int, error) {

	db := o.db.Conn()
	defer db.Close()
	var data []model.Dic

	query := "deleted_at is null"

	if body.Search != "" {
		query += " and name ='" + body.Search + "'"
	}

	offset := (body.Offset - 1) * body.Limit
	err := db.Model(&model.Dic{}).OrderExpr("id ASC").
		Where(query).
		Limit(body.Limit).
		Offset(offset).
		Select(&data)

	if err != nil {
		fmt.Println(err)
		return data, 0, err
	}

	count, err := db.Model(&model.Dic{}).Count()

	if err != nil {
		return data, 0, err
	}

	return data, count, nil
}

func (o *dic) GetDetail(ctx context.Context, ID int64) (model.Dic, error) {

	db := o.db.Conn()
	defer db.Close()

	dic := model.Dic{
		ID: ID,
	}

	err := db.Model(&dic).WherePK().First()
	if err != nil {
		return dic, err
	}

	return dic, nil
}

func (o *dic) GetCode(ctx context.Context) ([]model.DataMasterCodeOrganizerDic, error) {

	data := make([]model.DataMasterCodeOrganizerDic, 0)

	db := o.db.Conn()
	defer db.Close()

	err := db.Model(&model.Dic{}).OrderExpr("id ASC").Column("id", "name", "code").Select(&data)

	if err != nil {
		return data, err
	}

	return data, nil
}
