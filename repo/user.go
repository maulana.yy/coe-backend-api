package repo

import (
	"context"
	"fmt"
	"mymodule/db"
	"mymodule/model"
	"strings"
	"time"

	"github.com/go-pg/pg"
	"github.com/lib/pq"
)

type user struct {
	db *pg.DB
}

func NewUser(db *pg.DB) db.User {
	return &user{
		db: db,
	}
}

func (u *user) Create(ctx context.Context, body *model.User) error {

	db := u.db.Conn()
	defer db.Close()

	_, err := db.Model(body).Insert()

	return err
}

func (u *user) Update(ctx context.Context, body model.UserUpdate) error {

	db := u.db.Conn()
	defer db.Close()

	var newData model.User

	err := db.Model(&model.User{
		ID: body.ID,
	}).WherePK().Select(&newData)

	newData.Email = body.Email
	newData.Info.Name = body.Name
	newData.Info.Department = body.Department
	newData.Info.Level = body.Level
	newData.Info.Role = body.Role

	if body.Profile != "" {
		newData.Info.Profile = body.Profile
	}

	newData.UpdatedAt = pq.NullTime{
		Time:  time.Now(),
		Valid: true,
	}

	_, err = db.Model(&newData).WherePK().Update()

	return err
}

func (u *user) Delete(ctx context.Context, ID int64) error {

	db := u.db.Conn()
	defer db.Close()

	data := &model.User{
		ID: ID,
	}

	_, err := db.Model(data).WherePK().Delete()

	return err
}

func (u *user) Get(ctx context.Context, body model.GetAllReq) ([]model.User, int, error) {
	db := u.db.Conn()
	defer db.Close()
	var data []model.User
	offset := (body.Offset - 1) * body.Limit

	query := "deleted_at is null"

	if body.Search != "" {
		query += " and LOWER(username) LIKE '%" + body.Search + "%' or LOWER(email) LIKE '%" + body.Search + "%' or "
		query += " LOWER(info ->> 'name') LIKE '%" + body.Search + "%' or LOWER(info ->> 'role') LIKE '%" + body.Search + "%' or"
		query += " LOWER(info ->> 'department') LIKE '%" + body.Search + "%' or LOWER(info ->> 'level') LIKE '%" + body.Search + "%'"
	}

	fmt.Println(query)
	err := db.Model(&model.User{}).OrderExpr("id ASC").
		Where(query).
		Limit(body.Limit).
		Offset(offset).
		Select(&data)

	if err != nil {
		return data, 0, err
	}

	count, err := db.Model(&model.User{}).Count()

	if err != nil {
		return data, 0, err
	}
	return data, count, nil
}

func (u *user) GetDetail(ctx context.Context, ID int64) (*model.User, error) {
	db := u.db.Conn()
	defer db.Close()

	user := &model.User{
		ID: ID,
	}

	err := db.Model(user).WherePK().Column("username", "email", "info").First()
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (u *user) GetByUsername(ctx context.Context, username string) (model.User, error) {

	user := new(model.User)

	db := u.db.Conn()
	defer db.Close()

	err := db.Model(user).Where("LOWER(username) = ?", strings.ToLower(username)).Select()

	if err != nil {
		return *user, err
	}

	return *user, nil
}

func (u *user) GetCode(ctx context.Context, department []string) ([]model.UserCode, error) {
	data := make([]model.UserCode, 0)

	db := u.db.Conn()
	defer db.Close()

	query := "deleted_at is null"
	if len(department) > 0 && strings.Join(department, ",") != "" {

		departmentString := ""

		for x, dpt := range department {
			if x < len(department)-1 {
				departmentString += "'" + dpt + "',"
			} else {
				departmentString += "'" + dpt + "'"
			}
		}

		query += "info ->> 'department' IN (" + departmentString + ")"
	}

	err := db.Model(&model.User{}).Where(query).OrderExpr("id ASC").Column("id", "username").Select(&data)

	if err != nil {
		return data, err
	}

	return data, nil
}
