package repo

import (
	"context"
	"fmt"
	"mymodule/db"
	"mymodule/model"
	"strings"
	"time"

	"github.com/go-pg/pg"
	"github.com/lib/pq"
)

type participant struct {
	db *pg.DB
}

func NewParticipant(db *pg.DB) db.Participant {
	return &participant{
		db: db,
	}
}

func (p *participant) Create(ctx context.Context, body *model.Participant) error {

	db := p.db.Conn()
	defer db.Close()
	body.Name = strings.ToUpper(body.Name)
	_, err := db.Model(body).Insert()

	return err
}

func (p *participant) Update(ctx context.Context, body model.Participant) error {

	db := p.db.Conn()
	defer db.Close()

	var newData model.Participant

	err := db.Model(&model.Participant{
		ID: body.ID,
	}).WherePK().Select(&newData)

	if err != nil {
		return err
	}

	newData.Email = body.Email
	newData.Name = strings.ToUpper(body.Name)
	newData.Organizer = body.Organizer
	newData.Level = body.Level

	newData.UpdatedAt = pq.NullTime{
		Time:  time.Now(),
		Valid: true,
	}

	_, err = db.Model(&newData).WherePK().Update()

	return err
}

func (p *participant) Delete(ctx context.Context, ID int64) error {

	db := p.db.Conn()
	defer db.Close()

	data := &model.Participant{
		ID: ID,
	}

	_, err := db.Model(data).WherePK().Delete()

	return err
}

func (p *participant) Get(ctx context.Context, body model.GetAllReq) ([]model.Participant, int, error) {
	db := p.db.Conn()
	defer db.Close()
	var data []model.Participant
	offset := (body.Offset - 1) * body.Limit

	query := "deleted_at is null"

	if body.Search != "" {
		query += " and LOWER(name) LIKE '%" + body.Search + "%' or LOWER(email) LIKE '%" + body.Search + "%' or"
		query += " LOWER(organizer) LIKE '%" + body.Search + "%' or LOWER(level) LIKE '%" + body.Search + "%'"
	}
	err := db.Model(&model.Participant{}).OrderExpr("id ASC").
		Where(query).
		Limit(body.Limit).
		Offset(offset).
		Select(&data)

	if err != nil {
		return data, 0, err
	}

	count, err := db.Model(&model.Participant{}).Count()

	if err != nil {
		return data, 0, err
	}
	return data, count, nil
}

func (p *participant) GetDetail(ctx context.Context, ID int64) (*model.Participant, error) {
	db := p.db.Conn()
	defer db.Close()

	user := &model.Participant{
		ID: ID,
	}

	err := db.Model(user).WherePK().First()
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (p *participant) GetDByEmail(ctx context.Context, email string) (*model.Participant, error) {
	db := p.db.Conn()
	defer db.Close()

	user := &model.Participant{}

	err := db.Model(user).Where("email = ?", email).First()
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (p *participant) GetCode(ctx context.Context, organizer, level string) ([]model.ParticipantCode, error) {

	data := make([]model.ParticipantCode, 0)
	query := "deleted_at is null"

	if organizer != "" {
		splitOrg := strings.Split(organizer, ",")
		if len(splitOrg) > 1 {
			organizer = strings.Join(splitOrg, "','")
			query += " and organizer IN('" + organizer + "')"
		} else {
			query += " and organizer = '" + organizer + "'"
		}
	}

	if level != "" {
		splitLevel := strings.Split(level, ",")
		if len(splitLevel) > 1 {
			level = strings.Join(splitLevel, "','")
			query += " and level IN('" + level + "')"
		} else {
			query += " and level = '" + level + "'"
		}
	}

	db := p.db.Conn()
	defer db.Close()

	fmt.Println("QUERY GET CODE PARTICIPANT " + query)
	err := db.Model(&model.Participant{}).Where(query).OrderExpr("id ASC").Column("id", "name").Select(&data)

	if err != nil {
		return data, err
	}

	return data, nil
}
