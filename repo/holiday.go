package repo

import (
	"context"
	"fmt"
	"mymodule/db"
	"mymodule/model"
	"time"

	"github.com/go-pg/pg"
	"github.com/lib/pq"
)

type holiday struct {
	db *pg.DB
}

func NewHoliday(db *pg.DB) db.Holiday {
	return &holiday{
		db: db,
	}
}

func (h *holiday) Create(ctx context.Context, body model.Holiday) error {

	db := h.db.Conn()
	defer db.Close()

	_, err := db.Model(&body).Insert()

	return err
}

func (h *holiday) Update(ctx context.Context, body model.Holiday) error {

	db := h.db.Conn()
	defer db.Close()

	var newData model.Holiday

	err := db.Model(&model.Holiday{
		ID: body.ID,
	}).WherePK().Select(&newData)

	newData.Name = body.Name
	newData.HolidayDate = body.HolidayDate
	newData.Category = body.Category
	newData.UpdatedAt = pq.NullTime{
		Time:  time.Now(),
		Valid: true,
	}

	_, err = db.Model(&newData).WherePK().Update()

	return err
}

func (h *holiday) Delete(ctx context.Context, ID int64) error {

	db := h.db.Conn()
	defer db.Close()

	data := &model.Holiday{
		ID: ID,
	}

	_, err := db.Model(data).WherePK().Delete()

	return err
}

func (h *holiday) Get(ctx context.Context, body model.GetAllReq) ([]model.Holiday, int, error) {

	db := h.db.Conn()
	defer db.Close()
	var data []model.Holiday
	offset := (body.Offset - 1) * body.Limit

	query := "deleted_at is null"

	if body.Search != "" {
		query += " and LOWER(name) LIKE '%" + body.Search + "%' or LOWER(category) LIKE '%" + body.Search + "%'"
	}

	err := db.Model(&model.Holiday{}).OrderExpr("id ASC").
		Where(query).
		Limit(body.Limit).
		Offset(offset).
		Select(&data)

	if err != nil {
		return data, 0, err
	}

	count, err := db.Model(&model.Holiday{}).Count()

	if err != nil {
		return data, 0, err
	}

	return data, count, nil
}

func (h *holiday) GetDetail(ctx context.Context, ID int64) (model.Holiday, error) {

	db := h.db.Conn()
	defer db.Close()

	holiday := model.Holiday{
		ID: ID,
	}

	err := db.Model(&holiday).WherePK().First()
	if err != nil {
		return holiday, err
	}

	return holiday, nil
}

func (h *holiday) GetCode(ctx context.Context) ([]model.DataMasterCode, error) {

	data := make([]model.DataMasterCode, 0)

	db := h.db.Conn()
	defer db.Close()

	err := db.Model(&model.Holiday{}).OrderExpr("id ASC").Column("id", "name").Select(&data)

	if err != nil {
		return data, err
	}

	return data, nil
}

func (h *holiday) GetCalendar(ctx context.Context, calendar, startDate, endDate string, index int) ([]model.Holiday, error) {

	var err error
	db := h.db.Conn()
	defer db.Close()
	var data []model.Holiday

	query := "EXTRACT(YEAR FROM holiday.holiday_date) =" + fmt.Sprintf("%d", time.Now().Year())

	if startDate != "" && endDate != "" {
		query += " and holiday.holiday_date between '" + startDate + "' and '" + endDate + "'"
	}

	// if calendar == "week" {
	// 	query += " and EXTRACT(WEEK FROM holiday_date) =" + fmt.Sprintf("%d", index)
	// }

	// if calendar == "month" {
	// 	query += " and EXTRACT(MONTH FROM holiday_date) =" + fmt.Sprintf("%d", index)
	// }

	err = db.Model(&model.Holiday{}).OrderExpr("holiday.id ASC").
		ColumnExpr("holiday.id,holiday.name,holiday.category,holiday.holiday_date,categories.color").
		Join("INNER JOIN categories ON categories.name = holiday.category").
		Where(query).
		Select(&data)

	if err != nil {
		fmt.Println("ERROR : ", err)
		return data, err
	}

	return data, nil
}
