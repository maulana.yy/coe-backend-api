package repo

import (
	"context"
	"mymodule/db"
	"mymodule/model"
	"time"

	"github.com/go-pg/pg"
	"github.com/lib/pq"
)

type information struct {
	db *pg.DB
}

func NewInformation(db *pg.DB) db.Information {
	return &information{
		db: db,
	}
}

func (d *information) Create(ctx context.Context, body model.Information) error {

	db := d.db.Conn()
	defer db.Close()

	_, err := db.Model(&body).Insert()

	return err
}

func (d *information) Update(ctx context.Context, body model.Information) error {

	db := d.db.Conn()
	defer db.Close()

	var newData model.Information

	err := db.Model(&model.Information{
		ID: body.ID,
	}).WherePK().Select(&newData)

	newData.Title = body.Title
	newData.Description = body.Description
	newData.UpdatedAt = pq.NullTime{
		Time:  time.Now(),
		Valid: true,
	}

	_, err = db.Model(&newData).WherePK().Update()

	return err
}

func (d *information) Delete(ctx context.Context, ID int64) error {

	db := d.db.Conn()
	defer db.Close()

	data := &model.Information{
		ID: ID,
	}

	_, err := db.Model(data).WherePK().Delete()

	return err
}

func (d *information) GetInfoDashboard(ctx context.Context) ([]model.Information, int, error) {

	db := d.db.Conn()
	defer db.Close()
	var data []model.Information

	query := "deleted_at is null and status = true"

	err := db.Model(&model.Information{}).OrderExpr("created_at DESC").
		Where(query).
		Limit(5).
		Offset(0).
		Select(&data)

	if err != nil {
		return data, 0, err
	}

	count, err := db.Model(&model.Information{}).Count()

	if err != nil {
		return data, 0, err
	}
	return data, count, nil
}

func (d *information) Get(ctx context.Context, body model.GetAllReq) ([]model.Information, int, error) {

	db := d.db.Conn()
	defer db.Close()
	var data []model.Information

	query := "deleted_at is null"
	if body.Search == "status" {
		query += " and status = true"
	}

	if body.StartDate != "" && body.EndDate != "" {
		query += " and created_at between '" + body.StartDate + "' AND '" + body.EndDate + "'"
	}

	if body.Search != "" {
		query += " and LOWER(title) LIKE '%" + body.Search + "%'"
	}

	err := db.Model(&model.Information{}).OrderExpr("id ASC").
		Where(query).
		Limit(body.Limit).
		Offset(body.Offset - 1).
		Select(&data)

	if err != nil {
		return data, 0, err
	}

	count, err := db.Model(&model.Information{}).Count()

	if err != nil {
		return data, 0, err
	}
	return data, count, nil
}

func (d *information) GetDetail(ctx context.Context, ID int64) (model.Information, error) {

	db := d.db.Conn()
	defer db.Close()

	information := model.Information{
		ID: ID,
	}

	err := db.Model(&information).WherePK().First()
	if err != nil {
		return information, err
	}

	return information, nil
}

func (e *information) ChangeStatus(ctx context.Context, ID int64) error {
	approve := true
	db := e.db.Conn()
	defer db.Close()
	information := model.Information{
		ID: ID,
	}

	err := db.Model(&information).Column("id", "status").WherePK().First()
	if err != nil {
		return err
	}

	if information.Status {
		approve = false
	} else {
		approve = true
	}

	NewInformation := model.Information{
		ID:     ID,
		Status: approve,
	}
	_, err = db.Model(&NewInformation).Set("status = ?status").Where("id = ?id").Update()
	if err != nil {
		return err
	}

	return nil
}
