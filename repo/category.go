package repo

import (
	"context"
	"mymodule/db"
	"mymodule/model"
	"time"

	"github.com/go-pg/pg"
	"github.com/lib/pq"
)

type category struct {
	db *pg.DB
}

func NewCategory(db *pg.DB) db.Category {
	return &category{
		db: db,
	}
}

func (c *category) Create(ctx context.Context, body model.Category) error {

	db := c.db.Conn()
	defer db.Close()

	_, err := db.Model(&body).Insert()

	return err
}

func (c *category) Update(ctx context.Context, body model.Category) error {

	db := c.db.Conn()
	defer db.Close()

	var newData model.Category

	err := db.Model(&model.Category{
		ID: body.ID,
	}).WherePK().Select(&newData)

	newData.Name = body.Name
	newData.Color = body.Color
	newData.UpdatedAt = pq.NullTime{
		Time:  time.Now(),
		Valid: true,
	}

	_, err = db.Model(&newData).WherePK().Update()

	return err
}

func (c *category) Delete(ctx context.Context, ID int64) error {

	db := c.db.Conn()
	defer db.Close()

	data := &model.Category{
		ID: ID,
	}

	_, err := db.Model(data).WherePK().Delete()

	return err
}

func (c *category) Get(ctx context.Context, body model.GetAllReq) ([]model.Category, int, error) {

	db := c.db.Conn()
	defer db.Close()
	var data []model.Category

	offset := (body.Offset - 1) * body.Limit

	query := "deleted_at is null"

	if body.Search != "" {
		query += " and LOWER(name) LIKE '%" + body.Search + "%'"
	}

	err := db.Model(&model.Category{}).OrderExpr("id ASC").
		Where(query).
		Limit(body.Limit).
		Offset(offset).
		Select(&data)

	count, err := db.Model(&model.Category{}).Count()

	if err != nil {
		return data, 0, err
	}
	return data, count, nil
}

func (c *category) GetDetail(ctx context.Context, ID int64) (model.Category, error) {
	db := c.db.Conn()
	defer db.Close()

	category := model.Category{
		ID: ID,
	}

	err := db.Model(&category).WherePK().First()
	if err != nil {
		return category, err
	}

	return category, nil
}

func (c *category) GetByName(ctx context.Context, Name string) (model.Category, error) {
	db := c.db.Conn()
	defer db.Close()

	category := model.Category{}

	query := "name = '" + Name + "'"
	err := db.Model(&category).Where(query).First()
	if err != nil {
		return category, err
	}

	return category, nil
}

func (c *category) GetCode(ctx context.Context) ([]model.DataMasterCode, error) {
	data := make([]model.DataMasterCode, 0)

	db := c.db.Conn()
	defer db.Close()

	err := db.Model(&model.Category{}).OrderExpr("id ASC").Column("id", "name").Select(&data)

	if err != nil {
		return data, err
	}

	return data, nil
}
