package repo

import (
	"context"
	"mymodule/db"
	"mymodule/model"
	"time"

	"github.com/go-pg/pg"
	"github.com/lib/pq"
)

type role struct {
	db *pg.DB
}

func NewRole(db *pg.DB) db.Role {
	return &role{
		db: db,
	}
}

func (d *role) Create(ctx context.Context, body model.Role) error {

	db := d.db.Conn()
	defer db.Close()

	_, err := db.Model(&body).Insert()

	return err
}

func (d *role) Update(ctx context.Context, body model.Role) error {

	db := d.db.Conn()
	defer db.Close()

	var newData model.Role

	err := db.Model(&model.Role{
		ID: body.ID,
	}).WherePK().Select(&newData)

	newData.Name = body.Name
	newData.UpdatedAt = pq.NullTime{
		Time:  time.Now(),
		Valid: true,
	}

	_, err = db.Model(&newData).WherePK().Update()

	return err
}

func (d *role) Delete(ctx context.Context, ID int64) error {

	db := d.db.Conn()
	defer db.Close()

	data := &model.Role{
		ID: ID,
	}

	_, err := db.Model(data).WherePK().Delete()

	return err
}

func (d *role) Get(ctx context.Context, body model.GetAllReq) ([]model.Role, int, error) {

	db := d.db.Conn()
	defer db.Close()
	var data []model.Role

	query := "deleted_at is null"
	if body.Search != "" {
		query += " and lower(name) LIKE '%" + body.Search + "%'"
	}

	offset := (body.Offset - 1) * body.Limit
	err := db.Model(&model.Role{}).OrderExpr("id ASC").
		Where(query).
		Limit(body.Limit).
		Offset(offset).
		Select(&data)

	if err != nil {
		return data, 0, err
	}

	count, err := db.Model(&model.Role{}).Count()
	if err != nil {
		return data, 0, err
	}
	return data, count, nil
}

func (d *role) GetDetail(ctx context.Context, ID int64) (model.Role, error) {

	db := d.db.Conn()
	defer db.Close()

	role := model.Role{
		ID: ID,
	}

	err := db.Model(&role).WherePK().First()
	if err != nil {
		return role, err
	}

	return role, nil
}

func (d *role) GetCode(ctx context.Context) ([]model.DataMasterCode, error) {

	data := make([]model.DataMasterCode, 0)

	db := d.db.Conn()
	defer db.Close()

	err := db.Model(&model.Role{}).OrderExpr("id ASC").
		Column("id").
		ColumnExpr("upper(name)as name").Select(&data)

	if err != nil {
		return data, err
	}

	return data, nil
}

func (d *role) GetByName(ctx context.Context, name string) (model.Role, error) {

	db := d.db.Conn()
	defer db.Close()

	role := model.Role{}

	err := db.Model(&role).Where("name = ?", name).First()
	if err != nil {
		return role, err
	}

	return role, nil
}
