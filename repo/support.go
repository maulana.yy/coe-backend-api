package repo

import (
	"context"
	"mymodule/db"
	"mymodule/model"
	"time"

	"github.com/go-pg/pg"
	"github.com/lib/pq"
)

type support struct {
	db *pg.DB
}

func NewSupport(db *pg.DB) db.Support {
	return &support{
		db: db,
	}
}

func (s *support) Create(ctx context.Context, body model.Support) error {

	db := s.db.Conn()
	defer db.Close()

	_, err := db.Model(&body).Insert()

	return err
}

func (s *support) Update(ctx context.Context, body model.Support) error {

	db := s.db.Conn()
	defer db.Close()

	var newData model.Support

	err := db.Model(&model.Support{
		ID: body.ID,
	}).WherePK().Select(&newData)

	newData.Name = body.Name
	newData.Department = body.Department
	newData.UpdatedAt = pq.NullTime{
		Time:  time.Now(),
		Valid: true,
	}

	_, err = db.Model(&newData).WherePK().Update()

	return err
}

func (s *support) Delete(ctx context.Context, ID int64) error {

	db := s.db.Conn()
	defer db.Close()

	data := &model.Support{
		ID: ID,
	}

	_, err := db.Model(data).WherePK().Delete()

	return err
}

func (s *support) Get(ctx context.Context, body model.GetAllReq) ([]model.Support, int, error) {

	db := s.db.Conn()
	defer db.Close()
	var data []model.Support

	offset := (body.Offset - 1) * body.Limit
	err := db.Model(&model.Support{}).OrderExpr("id ASC").
		Limit(body.Limit).
		Offset(offset).
		Select(&data)

	if err != nil {
		return data, 0, err
	}

	count, err := db.Model(&model.Support{}).Count()
	if err != nil {
		return data, 0, err
	}
	return data, count, nil
}

func (s *support) GetDetail(ctx context.Context, ID int64) (model.Support, error) {

	db := s.db.Conn()
	defer db.Close()

	support := model.Support{
		ID: ID,
	}

	err := db.Model(&support).WherePK().First()
	if err != nil {
		return support, err
	}

	return support, nil
}

func (s *support) GetCode(ctx context.Context) ([]model.DataMasterCode, error) {

	data := make([]model.DataMasterCode, 0)

	db := s.db.Conn()
	defer db.Close()

	err := db.Model(&model.Support{}).OrderExpr("id ASC").Column("id", "name").Select(&data)

	if err != nil {
		return data, err
	}

	return data, nil
}
