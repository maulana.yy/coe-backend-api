package repo

import (
	"context"
	"fmt"
	"mymodule/db"
	"mymodule/model"
	"strings"
	"time"

	"github.com/go-pg/pg"
	"github.com/lib/pq"
)

type event struct {
	db *pg.DB
}

func NewEvent(db *pg.DB) db.Event {
	return &event{
		db: db,
	}
}

func (e *event) Create(ctx context.Context, body model.Event) error {

	db := e.db.Conn()
	defer db.Close()

	fmt.Println(body)
	_, err := db.Model(&body).Insert()
	fmt.Println("ERROR : ", err)
	return err
}

func (e *event) Update(ctx context.Context, body model.Event) error {

	db := e.db.Conn()
	defer db.Close()

	var newData model.Event

	err := db.Model(&model.Event{
		ID: body.ID,
	}).WherePK().Select(&newData)

	newData.Name = body.Name
	newData.Category = body.Category
	newData.Organizer = body.Organizer
	newData.Department = body.Department
	newData.Level = body.Level
	newData.Support = body.Support
	newData.Participant = body.Participant
	newData.Note = body.Note
	newData.Attachment = body.Attachment
	newData.UpdatedAt = pq.NullTime{
		Time:  time.Now(),
		Valid: true,
	}

	_, err = db.Model(&newData).WherePK().Update()

	return err
}

func (e *event) Delete(ctx context.Context, ID int64) error {

	db := e.db.Conn()
	defer db.Close()

	data := &model.Event{
		ID: ID,
	}

	_, err := db.Model(data).WherePK().Delete()

	return err
}

func (e *event) Get(ctx context.Context, body model.GetAllReq) ([]model.EventInfo, int, error) {

	var err error
	db := e.db.Conn()
	defer db.Close()
	var data []model.EventInfo

	query := "deleted_at is null"
	offset := (body.Offset - 1) * body.Limit
	if body.StartDate != "" && body.EndDate != "" {
		query += " and event_date_start between '" + body.StartDate + "' AND '" + body.EndDate + "'"
	}

	if body.Search != "" {
		query += " and LOWER(name) LIKE '%" + strings.ToLower(body.Search) + "%' or  LOWER(category ->> 'name') LIKE '%" + strings.ToLower(body.Search) + "%'"
		query += " and LOWER(department) LIKE '%" + strings.ToLower(body.Search) + "%' or  LOWER(organizer) LIKE '%" + strings.ToLower(body.Search) + "%'"
	}

	fmt.Println("QUERY")
	fmt.Println(query)
	if body.Limit > 0 && body.Offset > 0 {
		err = db.Model(&model.Event{}).OrderExpr("id ASC").
			ColumnExpr("id,name,category->>'name' as category,organizer,department,event_date_start,event_date_end,event_time_start,event_time_end,status,approved").
			Where(query).
			Limit(body.Limit).
			Offset(offset).
			Select(&data)

	} else {
		err = db.Model(&model.Event{}).OrderExpr("id ASC").
			ColumnExpr("id,name,category->>'link' as category,organizer,department,event_date_start,event_time_start,event_time_end,status,approved").
			Where(query).
			Select(&data)
	}

	if err != nil {
		return data, 0, err
	}

	count, err := db.Model(&model.Event{}).OrderExpr("id ASC").Count()
	if err != nil {
		return data, 0, err
	}
	return data, count, nil
}

func (e *event) GetDetail(ctx context.Context, ID int64) (model.Event, error) {

	db := e.db.Conn()
	defer db.Close()

	event := model.Event{
		ID: ID,
	}

	err := db.Model(&event).WherePK().First()
	if err != nil {
		return event, err
	}

	return event, nil
}

func (e *event) UpdateApproval(ctx context.Context, ID int64, approval string) error {
	db := e.db.Conn()
	defer db.Close()
	event := model.Event{
		ID: ID,
	}

	err := db.Model(&event).Column("id", "approved").WherePK().First()
	if err != nil {
		return err
	}

	fmt.Println("STATUS : ", approval)

	newEvent := model.Event{
		ID:       ID,
		Approved: approval,
	}

	_, err = db.Model(&newEvent).Set("approved = ?approved").Where("id = ?id").Update()
	if err != nil {
		return err
	}

	return nil
}

func (e *event) UpdateStatus(ctx context.Context, ID int64, status, newDateStart, newDateEnd, newTimeStart, newTimeEnd string) error {

	db := e.db.Conn()
	defer db.Close()

	event := model.Event{
		ID:     ID,
		Status: status,
	}

	set := "status = ?status"

	if newDateStart != "" && newDateEnd != "" && newTimeStart != "" && newTimeEnd != "" {
		layoutFormat := "2006-01-02"

		startDate, _ := time.Parse(layoutFormat, newDateStart)
		endDate, _ := time.Parse(layoutFormat, newDateEnd)
		event.EventDateStart = startDate
		event.EventDateEnd = endDate
		event.EventTimeStart = newTimeStart
		event.EventTimeEnd = newTimeEnd
		set += ",event_date_start = ?event_date_start, event_date_end = ?event_date_end, event_time_start = ?event_time_start, event_time_end = ?event_time_end"
	}

	_, err := db.Model(&event).Set(set).Where("id = ?id").Update()
	if err != nil {
		return err
	}

	return nil
}

func (e *event) CountByStatus(ctx context.Context, Status string, Month int) (int, error) {

	db := e.db.Conn()
	defer db.Close()

	query := "status ='" + Status + "'"

	if Month != 0 {
		query += " and EXTRACT(MONTH FROM event_date_start) =" + fmt.Sprintf("%d", Month) + " and EXTRACT(YEAR FROM event_date_start) =" + fmt.Sprintf("%d", time.Now().Year())
	}

	count, err := db.Model((*model.Event)(nil)).Where(query).Count()

	if err != nil {
		return 0, err
	}

	return count, nil
}

func (e *event) Today(ctx context.Context) ([]model.Event, error) {

	var err error
	db := e.db.Conn()
	defer db.Close()
	var data []model.Event
	tomorrow := time.Now().AddDate(0, 0, 1).Format("2006-01-02")
	fmt.Println("TODAY : ", time.Now().Format("2006-01-02"))
	fmt.Println("TOMORROW : ", tomorrow)

	query := "event_date_start BETWEEN '" + time.Now().Format("2006-01-02") + "' AND '" + tomorrow + "'"

	err = db.Model(&model.Event{}).OrderExpr("id ASC").
		Where(query).
		Limit(1).
		Select(&data)

	if err != nil {
		fmt.Println(err)
		return data, err
	}

	fmt.Println(data)
	return data, nil
}

func (e *event) GetCalendar(ctx context.Context, calendar, startDate, endDate string, index int) ([]model.EventInfo, error) {

	var err error
	db := e.db.Conn()
	defer db.Close()
	var data []model.EventInfo

	// query := "EXTRACT(YEAR FROM events.event_date_start) =" + fmt.Sprintf("%d", time.Now().Year())

	// if startDate != "" && endDate != "" {
	// 	query += " and events.event_date_start >= '" + startDate + "' and events.event_date_end <= '" + endDate + "'"
	// }

	// if calendar == "week" {
	// 	query += " and EXTRACT(WEEK FROM event_date_start) =" + fmt.Sprintf("%d", index)
	// }

	// if calendar == "month" {
	// 	query += " and EXTRACT(MONTH FROM event_date_start) =" + fmt.Sprintf("%d", index)
	// }

	fmt.Println("YEAR : ", time.Now().Year())
	fmt.Println("TIME START : ", startDate)
	fmt.Println("TIME END : ", endDate)

	_, err = db.Query(&data, `select events.id,events.name,events.category->>'name' as category,events.organizer,events.department,events.event_date_start,events.event_date_end,events.event_time_start,events.event_time_end,events.status,events.approved,categories.color from events JOIN categories ON categories.name = events.category->>'name' where 
	EXTRACT(YEAR FROM events.event_date_start) = ? and events.event_date_start >= ? and events.event_date_end <= ? and events.status != 'Cancel' and events.approved = 'APPROVED' order by events.id asc`, time.Now().Year(), startDate, endDate)

	// err = db.Model(&model.Event{}).Query(z)
	if err != nil {
		fmt.Println("ERROR : ", err)
		return data, err
	}

	return data, nil
}
