package app

import (
	"mymodule/db"
	"mymodule/repo"

	"github.com/go-pg/pg"
)

type Repo struct {
	UserDB        db.User
	CategoryDB    db.Category
	OrganizerDB   db.Organizer
	RoleDB        db.Role
	EventDB       db.Event
	HolidayDB     db.Holiday
	SupportDB     db.Support
	InformationDB db.Information
	DicDB         db.Dic
	ParticipantDB db.Participant
}

func NewRepo(db *pg.DB) *Repo {
	return &Repo{
		UserDB:        repo.NewUser(db),
		CategoryDB:    repo.NewCategory(db),
		OrganizerDB:   repo.NewOrganizer(db),
		RoleDB:        repo.NewRole(db),
		EventDB:       repo.NewEvent(db),
		HolidayDB:     repo.NewHoliday(db),
		SupportDB:     repo.NewSupport(db),
		InformationDB: repo.NewInformation(db),
		DicDB:         repo.NewDic(db),
		ParticipantDB: repo.NewParticipant(db),
	}
}
