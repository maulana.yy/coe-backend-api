package app

import (
	"context"
	"mymodule/constant"
	"mymodule/model"
	"mymodule/pkg"
	"net/http"
	"strconv"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/rs/zerolog"
)

type Middleware struct {
	Config  *model.Config
	Usecase Usecase
	Log     *zerolog.Logger
}

// New to create new middleware
func NewMiddleware(uc Usecase, cfg *model.Config, log *zerolog.Logger) *Middleware {
	return &Middleware{
		Config:  cfg,
		Log:     log,
		Usecase: uc,
	}
}

func badRequest(c echo.Context, msg string) error {
	return c.JSON(http.StatusBadRequest, model.ResponseJSON{
		Success: false,
		Message: msg,
	})
}

func (m *Middleware) MiddlewareLogging(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		globalV := pkg.NewGlobalValue()
		c.Set(constant.GlobalCtx, globalV)

		userid, err := strconv.Atoi(c.Request().Header.Get("User-Id"))
		if err != nil {
			userid = 0
		}

		m.Log.Info().Fields(map[string]interface{}{
			"request_id":  globalV.GetRequestID(),
			"time":        globalV.GetRequestTime().Format(time.RFC3339),
			"status_code": c.Response().Status,
			"method":      c.Request().Method,
			"uri":         c.Request().URL.String(),
			"ip":          c.Request().RemoteAddr,
			"user_agent":  c.Request().UserAgent(),
			"user_id":     userid,
			"type":        "incoming request",
		}).Msg("")
		return next(c)
	}
}

func (m *Middleware) Recover(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {

		defer func() {
			if r := recover(); r != nil {
				m.Log.Error().Fields(map[string]interface{}{
					"time": time.Now().Format(time.RFC3339),
					"msg":  r,
					"type": "service panic",
				}).Msg("")
			}
		}()

		return next(c)
	}
}

func (m *Middleware) MiddlewareToken(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		req := c.Request()

		userID := req.Header.Get(constant.UserIDHeader)
		if userID == "" {
			return badRequest(c, "please include User-Id in header")
		}
		userIDInt, err := strconv.ParseInt(userID, 10, 64)
		if err != nil {
			return badRequest(c, err.Error())
		}
		c.Set(constant.UserIDCtx, userIDInt)

		if m.Config.Rest.TokenValidate {
			token := req.Header.Get(constant.TokenHeader)
			if token == "" {
				return badRequest(c, "please include COE-Token in header")
			}

			ctx := context.Background()
			user, err := m.Usecase.UserGetByID(ctx, userIDInt)
			if err != nil {
				return badRequest(c, "user not found")
			}

			if user.Token == "" {
				return badRequest(c, "please login first")
			}

			if user.Token != token {
				return badRequest(c, "invalid token, please relogin")
			}

			if (user.Info.TokenGeneratedTime + m.Config.Rest.TokenExpire) < time.Now().Unix() {
				return badRequest(c, "expired token, please relogin")
			}

			go m.Usecase.UserUpdateInfo(ctx, model.UserUpdateInfoReq{
				UserID:       user.ID,
				TokenGenTime: time.Now().Unix(),
			})
		}
		return next(c)
	}
}
