package app

import (
	"io"
	"os"
	"time"

	"github.com/rs/zerolog"
)

func InitLogger() (*zerolog.Logger, error) {
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnixMs
	logFile, err := checkLogFile()
	if err != nil {
		return nil, err
	}

	zLog := zerolog.New(logFile)
	zLog.Info().Fields(map[string]interface{}{
		"time": time.Now().Format(time.RFC3339),
	})
	return &zLog, nil
}

func checkLogFile() (io.Writer, error) {
	currentDate := time.Now().Format("2006-01-02")

	folderPath := "./logs/"
	if _, err := os.Stat(folderPath); os.IsNotExist(err) {
		os.Mkdir(folderPath, 0755)
	}

	filePath := folderPath + currentDate + ".log"

	file, err := os.OpenFile(filePath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return nil, err
	}
	return file, nil
}
