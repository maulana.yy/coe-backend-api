package app

import (
	"context"
	"mymodule/model"
)

type Usecase interface {
	//Dashboard
	Calendar(ctx context.Context) ([]model.Event, error)
	GroupingEvent(ctx context.Context) ([]model.GroupEvent, error)
	Graph(ctx context.Context) ([]map[string]interface{}, error)
	GetEventToday(ctx context.Context) ([]model.EventInfo, error)
	GetInfo(ctx context.Context) ([]model.Information, error)
	DonutChart(ctx context.Context) (map[string]int, error)

	//User
	Login(ctx context.Context, req model.Login) (model.User, error)
	UserAll(ctx context.Context, req model.GetAllReq) ([]model.UserInfo, int, error)
	UserGetCode(ctx context.Context, department []string) ([]model.UserCode, error)
	UserGetByID(ctx context.Context, userID int64) (*model.User, error)
	UserCreate(ctx context.Context, body model.User) error
	UserUpdate(ctx context.Context, body model.UserUpdate) error
	UserDelete(ctx context.Context, userID int64) error
	UserUpdateInfo(ctx context.Context, info model.UserUpdateInfoReq) (*model.User, error)

	//Event
	EventAll(ctx context.Context, req model.GetAllReq) ([]model.EventInfo, int, error)
	EventByID(ctx context.Context, ID int64) (model.Event, error)
	EventCreate(ctx context.Context, body model.EventRequest) error
	EventUpdate(ctx context.Context, body model.EventRequest, id int64) error
	EventDelete(ctx context.Context, ID int64) error
	EventGetReport(ctx context.Context) (string, error)
	EventUpdateStatus(ctx context.Context, ID int64, status, newDateStart, newDateEnd, newTimeStart, newTimeEnd string) error
	EventApprove(ctx context.Context, ID int64, approval string) error
	EventToday(ctx context.Context) (model.EventDetail, error)
	EventOfCalendar(ctx context.Context, calendar, startDate, endDate string) ([]model.EventTime, error)

	//Category
	CategoryAll(ctx context.Context, req model.GetAllReq) ([]model.Category, int, error)
	CategoryGetCode(ctx context.Context) ([]model.DataMasterCode, error)
	CategoryByID(ctx context.Context, ID int64) (model.Category, error)
	CategoryCreate(ctx context.Context, body model.Category) error
	CategoryUpdate(ctx context.Context, body model.Category) error
	CategoryDelete(ctx context.Context, ID int64) error

	//Organizer
	OrganizerAll(ctx context.Context, req model.GetAllReq) ([]model.Organizer, int, error)
	OrganizerGetCode(ctx context.Context) ([]model.DataMasterCodeOrganizerDic, error)
	OrganizerByID(ctx context.Context, ID int64) (model.Organizer, error)
	OrganizerCreate(ctx context.Context, body model.Organizer) error
	OrganizerUpdate(ctx context.Context, body model.Organizer) error
	OrganizerDelete(ctx context.Context, ID int64) error

	//DiC
	RoleAll(ctx context.Context, req model.GetAllReq) ([]model.Role, int, error)
	RoleGetCode(ctx context.Context) ([]model.DataMasterCode, error)
	RoleByID(ctx context.Context, ID int64) (model.Role, error)
	RoleCreate(ctx context.Context, body model.Role) error
	RoleUpdate(ctx context.Context, body model.Role) error
	RoleDelete(ctx context.Context, ID int64) error

	//Holiday
	HolidayAll(ctx context.Context, req model.GetAllReq) ([]model.HolidayRes, int, error)
	HolidayByID(ctx context.Context, ID int64) (model.Holiday, error)
	HolidayCreate(ctx context.Context, body model.HolidayReq) error
	HolidayUpdate(ctx context.Context, body model.HolidayReq) error
	HolidayDelete(ctx context.Context, ID int64) error

	//Support
	SupportAll(ctx context.Context, req model.GetAllReq) ([]model.Support, int, error)
	SupportGetCode(ctx context.Context) ([]model.DataMasterCode, error)
	SupportByID(ctx context.Context, ID int64) (model.Support, error)
	SupportCreate(ctx context.Context, body model.Support) error
	SupportUpdate(ctx context.Context, body model.Support) error
	SupportDelete(ctx context.Context, ID int64) error

	InformationAll(ctx context.Context, req model.GetAllReq) ([]model.Information, int, error)
	InformationByID(ctx context.Context, ID int64) (model.Information, error)
	InformationCreate(ctx context.Context, body model.Information) error
	InformationUpdate(ctx context.Context, body model.Information) error
	InformationDelete(ctx context.Context, ID int64) error
	InformationChangeStatus(ctx context.Context, ID int64) error

	DicAll(ctx context.Context, req model.GetAllReq) ([]model.Dic, int, error)
	DicGetCode(ctx context.Context) ([]model.DataMasterCodeOrganizerDic, error)
	DicByID(ctx context.Context, ID int64) (model.Dic, error)
	DicCreate(ctx context.Context, body model.Dic) error
	DicUpdate(ctx context.Context, body model.Dic) error
	DicDelete(ctx context.Context, ID int64) error

	ParticipantCreate(ctx context.Context, data model.Participant) error
	ParticipantUpdate(ctx context.Context, data model.Participant) error
	ParticipantDelete(ctx context.Context, ID int64) error
	ParticipantGet(ctx context.Context, data model.GetAllReq) ([]model.Participant, int, error)
	ParticipantGetDetail(ctx context.Context, ID int64) (*model.Participant, error)
	ParticipantGetCode(ctx context.Context, organizer, level string) ([]model.ParticipantCode, error)
	InsertBulkCSV(ctx context.Context, filename string) error
}
