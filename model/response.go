package model

type GroupEvent struct {
	Name  string `json:"name"`
	Count int    `json:"count"`
}

type GraphEvent struct {
	Status string `json:"status"`
	Count  int    `json:"count"`
}

type DataMasterCode struct {
	ID   int64  `db:"id" json:"id"`
	Name string `db:"name" json:"name"`
}

type DataMasterCodeOrganizerDic struct {
	ID   int64  `db:"id" json:"id"`
	Name string `db:"name" json:"name"`
	Code string `db:"code" json:"code"`
}
