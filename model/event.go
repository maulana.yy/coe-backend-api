package model

import (
	"time"

	"github.com/lib/pq"
)

type Event struct {
	ID             int64              `db:"id" json:"id"`
	Name           string             `db:"name" json:"name"`
	Category       CategoryEvent      `json:"category_event"`
	Organizer      string             `db:"organizer" json:"organizer"`
	Department     []string           `db:"department" json:"departments" pg:",array"`
	Jabatan        []string           `db:"jabatan" json:"jabatan" pg:",array"`
	Level          []string           `db:"level" json:"level" pg:",array"`
	Support        SupportEvent       `db:"support" json:"support" `
	Participant    []ParticipantEvent `pg:",array" json:"participant_event"`
	Status         string             `db:"status" json:"status"`
	Approved       string             `db:"approved" json:"approved"`
	Duration       int                `db:"duration" json:"duration"`
	Note           string             `db:"note" json:"note"`
	Attachment     []Attachment       `db:"attachment" json:"attachment" pg:",array"`
	EventDateStart time.Time          `db:"event_date_start" json:"event_date_start"`
	EventDateEnd   time.Time          `db:"event_date_end" json:"event_date_end"`
	EventTimeStart string             `db:"event_time_start" json:"event_time_start"`
	EventTimeEnd   string             `db:"event_time_end" json:"event_time_end"`
	CreatedAt      time.Time          `db:"created_at" json:"created_at"`
	CreatedBy      string             `db:"created_by" json:"created_by"`
	UpdatedAt      pq.NullTime        `db:"updated_at" json:"-"`
	DeletedAt      pq.NullTime        `db:"deleted_at" json:"-"`
}

type EventInfo struct {
	ID             int64     `db:"id" json:"id"`
	Name           string    `db:"name" json:"name"`
	Category       string    `db:"category" json:"category"`
	Organizer      string    `db:"organizer" json:"organizer"`
	Department     []string  `db:"department" json:"department" pg:",array"`
	EventDateStart time.Time `db:"event_date_start" json:"event_date_start"`
	EventDateEnd   time.Time `db:"event_date_end" json:"event_date_end"`
	EventTimeStart string    `db:"event_time_start" json:"event_time_start"`
	EventTimeEnd   string    `db:"event_time_end" json:"event_time_end"`
	Status         string    `db:"status" json:"status"`
	Approved       string    `db:"approved" json:"approved"`
	Color          string    `db:"color" json:"color"`
}

type EventDetail struct {
	ID             int64              `db:"id" json:"id"`
	Name           string             `db:"name" json:"name"`
	Organizer      string             `db:"organizer" json:"organizer"`
	Participant    []ParticipantEvent `pg:",array" json:"participant_event"`
	Status         string             `db:"status" json:"status"`
	Approved       string             `db:"approved" json:"approved"`
	EventDateStart string             `db:"event_date_start" json:"event_date_start"`
	EventTimeStart string             `db:"event_time_start" json:"event_time_start"`
	EventTimeEnd   string             `db:"event_time_end" json:"event_time_end"`
	PicUsername    string             `db:"pic_username" json:"pic_username"`
	PicName        string             `json:"pic_name"`
	PicRole        string             `json:"pic_role"`
}

type EventTime struct {
	Title string `json:"title"`
	Start string `json:"start"`
	End   string `json:"end"`
	Color string `json:"color"`
}

type ParticipantEvent struct {
	Username string `json:"username"`
	Name     string `json:"name"`
}

type CategoryEvent struct {
	Name string `json:"name"`
	Room string `json:"room"`
	Link string `json:"link"`
}

type SupportEvent struct {
	Pic      string   `json:"pic"`
	ListItem []string `json:"list_item"`
}

type Attachment struct {
	Type string `json:"type"`
	Path string `json:"path"`
}
