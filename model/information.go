package model

import (
	"time"

	"github.com/lib/pq"
)

type Information struct {
	ID          int64       `db:"id" json:"id"`
	Title       string      `db:"title" json:"title"`
	Description string      `db:"description" json:"description"`
	Status      bool        `db:"status" json:"status"`
	CreatedAt   time.Time   `db:"created_at" json:"created_at"`
	CreatedBy   string      `db:"created_by" json:"created_by"`
	UpdatedAt   pq.NullTime `db:"updated_at" json:"-"`
	DeletedAt   pq.NullTime `db:"deleted_at" json:"-"`
}
