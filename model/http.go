package model

type ResponseJSON struct {
	Success bool        `json:"success"`
	Message string      `json:"message"`
	Data    interface{} `json:"data,omitempty"`
	Code    int         `json:"code,omitempty"`
}

type UserUpdateInfoReq struct {
	Username     string `json:"username"`
	Name         string `json:"name"`
	UserID       int64
	TokenGenTime int64
}

type GetAllReq struct {
	Search    string `json:"search"`
	Limit     int    `json:"limit"`
	Offset    int    `json:"offset"`
	StartDate string `json:"start_date"`
	EndDate   string `json:"end_date"`
}

type GetAllEventReq struct {
	Limit     int    `json:"limit"`
	Offset    int    `json:"offset"`
	StartDate string `json:"start_date"`
	EndDate   string `json:"end_date"`
}

type Login struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type UserUpdate struct {
	ID         int64  `db:"id" json:"id"`
	Email      string `db:"email" json:"email"`
	Name       string `json:"name"`
	Role       string `json:"role"`
	Department string `json:"department"`
	Level      string `json:"level"`
	Profile    string `json:"profile"`
}

type EventRequest struct {
	Name        string
	Category    string
	Organizer   string
	Room        string
	Link        string
	Department  []string
	Level       []string
	Pic         string
	StartDate   string
	EndDate     string
	StartTime   string
	EndTime     string
	Participant []string
	Note        string
	Support     []string
	Attachment  string
	Createdby   string
}
