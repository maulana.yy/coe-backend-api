package model

import (
	"time"

	"github.com/lib/pq"
)

type Support struct {
	ID         int64       `db:"id" json:"id"`
	Name       string      `db:"name" json:"name"`
	Department string      `db:"department" json:"department"`
	CreatedAt  time.Time   `db:"created_at" json:"created_at"`
	CreatedBy  string      `db:"created_by" json:"created_by"`
	UpdatedAt  pq.NullTime `db:"updated_at" json:"-"`
	DeletedAt  pq.NullTime `db:"deleted_at" json:"-"`
}
