package model

import (
	"time"

	"github.com/lib/pq"
)

type Holiday struct {
	ID          int64       `db:"id" json:"id"`
	Name        string      `db:"name" json:"name"`
	Category    string      `db:"category" json:"category"`
	Color       string      `db:"color" json:"color"`
	HolidayDate time.Time   `db:"holiday_date" json:"holiday_date"`
	CreatedAt   time.Time   `db:"created_at" json:"created_at"`
	CreatedBy   string      `db:"created_by" json:"created_by"`
	UpdatedAt   pq.NullTime `db:"updated_at" json:"-"`
	DeletedAt   pq.NullTime `db:"deleted_at" json:"-"`
}

type HolidayReq struct {
	ID          int64  `json:"id"`
	Name        string `json:"name"`
	Category    string `json:"category"`
	HolidayDate string `json:"holiday_date"`
	CreatedBy   string `json:"created_by"`
}

type HolidayRes struct {
	ID          int64     `db:"id" json:"id"`
	Name        string    `db:"name" json:"name"`
	Category    string    `db:"category" json:"category"`
	HolidayDate string    `json:"holiday_date"`
	CreatedAt   time.Time `db:"created_at" json:"created_at"`
}
