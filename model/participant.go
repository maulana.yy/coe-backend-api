package model

import (
	"time"

	"github.com/lib/pq"
)

type Participant struct {
	ID        int64       `db:"id" json:"id"`
	Name      string      `db:"name" json:"name"`
	Email     string      `db:"email" json:"email"`
	Organizer string      `db:"organizer" json:"organizer"`
	Level     string      `db:"level" json:"level"`
	CreatedAt time.Time   `db:"created_at" json:"created_at"`
	CreatedBy string      `db:"created_by" json:"created_by"`
	UpdatedAt pq.NullTime `db:"updated_at" json:"-"`
	DeletedAt pq.NullTime `db:"deleted_at" json:"-"`
}

type ParticipantCode struct {
	ID        int64       `db:"id" json:"id"`
	Name      string      `db:"name" json:"name"`
}
