package model

import (
	"time"

	"github.com/lib/pq"
)

type User struct {
	ID        int64       `db:"id" json:"id"`
	Username  string      `db:"username" json:"username"`
	Password  string      `db:"password" json:"password"`
	Token     string      `db:"token" json:"token"`
	Email     string      `db:"email" json:"email"`
	Info      Info        `db:"info" json:"info"`
	CreatedAt time.Time   `db:"created_at" json:"created_at"`
	CreatedBy string      `db:"created_by" json:"created_by"`
	UpdatedAt pq.NullTime `db:"updated_at" json:"-"`
	DeletedAt pq.NullTime `db:"deleted_at" json:"-"`
}

type UserInfo struct {
	ID         int64     `db:"id" json:"id"`
	Username   string    `db:"username" json:"username"`
	Email      string    `db:"email" json:"email"`
	Name       string    `json:"name"`
	Role       string    `json:"role"`
	Department string    `json:"department"`
	Level      string    `json:"level"`
	CreatedAt  time.Time `db:"created_at" json:"created_at"`
}
type Info struct {
	Name               string `json:"name"`
	Profile            string `json:"profile"`
	Role               string `json:"role"`
	Department         string `json:"department"`
	Level              string `json:"level"`
	TokenGeneratedTime int64  `json:"token_generated_time,omitempty"`
}

type UserCode struct {
	ID       int64  `db:"id" json:"id"`
	Username string `db:"username" json:"username"`
}
