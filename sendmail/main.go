package main

import (
	"fmt"
	"log"
	"mymodule/model"
	"mymodule/pkg"
	"net/smtp"
	"strings"
)

func main() {
	filePath := "./config.yaml"

	var cfg model.Config
	err := pkg.ReadFile(&cfg, filePath)
	if err != nil {
		fmt.Println("error read file")
		log.Fatal(err)
	}
	to := []string{"irpan.pamil@kalbenutritionals.com"}
	cc := []string{}
	subject := "Test mail"
	message := "Hello"

	CONFIG_SMTP_HOST := cfg.Smtp.Host
	CONFIG_SMTP_PORT := cfg.Smtp.Port
	CONFIG_SENDER_NAME := "Email Coe <" + cfg.Smtp.Email + ">"
	CONFIG_AUTH_EMAIL := cfg.Smtp.Email
	CONFIG_AUTH_PASSWORD := cfg.Smtp.Password

	fmt.Println("HOST : ", cfg.Smtp.Host)
	fmt.Println("PORT : ", cfg.Smtp.Port)
	fmt.Println("EMAIL : ", cfg.Smtp.Email)
	body := "From: " + CONFIG_SENDER_NAME + "\n" +
		"To: " + strings.Join(to, ",") + "\n" +
		"Cc: " + strings.Join(cc, ",") + "\n" +
		"Subject: " + subject + "\n\n" +
		message

	auth := smtp.PlainAuth("", CONFIG_AUTH_EMAIL, CONFIG_AUTH_PASSWORD, CONFIG_SMTP_HOST)
	smtpAddr := fmt.Sprintf("%s:%d", CONFIG_SMTP_HOST, CONFIG_SMTP_PORT)

	err = smtp.SendMail(smtpAddr, auth, CONFIG_AUTH_EMAIL, append(to, cc...), []byte(body))
	if err != nil {
		fmt.Println("ERROR : ", err.Error())
		log.Fatal(err.Error())
	}

	fmt.Println("MAIL SENT")
	log.Println("Mail sent!")

}
