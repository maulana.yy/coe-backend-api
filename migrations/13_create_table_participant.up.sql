CREATE TABLE IF NOT EXISTS public.participants(
    id bigserial NOT NULL,
    name varchar NOT NULL,
    email varchar NOT NULL,
    organizer varchar,
    level varchar,
    created_by varchar ,
    created_at timestamptz NOT NULL DEFAULT now(), 
    deleted_at timestamptz, 
    updated_at timestamptz
);DROP TABLE IF EXISTS public.roles CASCADE;