ALTER TABLE events 
    alter event_time_start type time without time zone,
    alter event_time_end type time without time zone,
