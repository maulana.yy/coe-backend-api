CREATE TABLE IF NOT EXISTS public.information(
    id bigserial NOT NULL,
    title varchar NOT NULL,
    description TEXT,
    status bool,
    created_by varchar ,
    created_at timestamptz NOT NULL DEFAULT now(), 
    deleted_at timestamptz, 
    updated_at timestamptz
);