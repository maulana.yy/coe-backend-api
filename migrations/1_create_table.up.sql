CREATE TABLE IF NOT EXISTS public.users(
    id bigserial NOT NULL,
    username varchar NOT NULL,
    password varchar NOT NULL,
    token varchar ,
    email varchar NOT NULL,
    info jsonb,
    created_by varchar ,
    created_at timestamptz NOT NULL DEFAULT now(), 
    deleted_at timestamptz, 
    updated_at timestamptz
);

CREATE TABLE IF NOT EXISTS public.categories(
    id bigserial NOT NULL,
    name varchar NOT NULL,
    created_by varchar ,
    created_at timestamptz NOT NULL DEFAULT now(), 
    deleted_at timestamptz, 
    updated_at timestamptz
);

CREATE TABLE IF NOT EXISTS public.organizers(
    id bigserial NOT NULL,
    code varchar NOT NULL,
    name varchar NOT NULL,
    created_by varchar ,
    created_at timestamptz NOT NULL DEFAULT now(), 
    deleted_at timestamptz, 
    updated_at timestamptz
);

CREATE TABLE IF NOT EXISTS public.holidays(
    id bigserial NOT NULL,
    name varchar NOT NULL,
    holiday_date timestamptz NOT NULL,
    created_by varchar ,
    created_at timestamptz NOT NULL DEFAULT now(), 
    deleted_at timestamptz, 
    updated_at timestamptz
);

CREATE TABLE IF NOT EXISTS public.supports(
    id bigserial NOT NULL,
    name varchar NOT NULL,
    department varchar NOT NULL,
    created_by varchar ,
    created_at timestamptz NOT NULL DEFAULT now(), 
    deleted_at timestamptz, 
    updated_at timestamptz
);

CREATE TABLE IF NOT EXISTS public.events(
    id bigserial NOT NULL,
    name varchar NOT NULL,
    category jsonb NOT NULL,
    organizer varchar NOT NULL,
    department varchar NOT NULL,
    level varchar[],
    support varchar[],
    participant jsonb[],
    status varchar,
    approved bool,
    duration int,
    note varchar,
    created_by varchar ,
    created_at timestamptz NOT NULL DEFAULT now(), 
    deleted_at timestamptz, 
    updated_at timestamptz
);

