ALTER TABLE events 
    alter column event_date type TIMESTAMP without time zone,
    add attachment jsonb[],
    add event_date_start TIMESTAMP without time zone,
    add event_date_end TIMESTAMP without time zone,
    add event_time_start time without time zone,
    add event_time_end time without time zone,
    drop column if EXISTS support,
    add support jsonb;
