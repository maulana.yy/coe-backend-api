ALTER TABLE events 
    drop column if EXISTS department,
    add department varchar;
 