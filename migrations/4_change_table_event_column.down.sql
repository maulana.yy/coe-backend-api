ALTER TABLE events 
    alter column event_date type timestamptz,
    drop column if EXISTS attachment,
    drop column if EXISTS event_date_end ,
    drop column if EXISTS event_time_start ,
    drop column if EXISTS event_time_end ,
    drop column if EXISTS support,
    add support varchar[];
