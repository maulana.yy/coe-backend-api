CREATE TABLE IF NOT EXISTS public.roles(
    id bigserial NOT NULL,
    name varchar NOT NULL,
    created_by varchar ,
    created_at timestamptz NOT NULL DEFAULT now(), 
    deleted_at timestamptz, 
    updated_at timestamptz
);