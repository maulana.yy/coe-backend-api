package service

import resty "github.com/go-resty/resty/v2"

type RequestInfo struct {
	Method string
	URL    string
	Token  string
}

func (r *RequestInfo) Prepare() *resty.Request {

	client := resty.New()

	req := client.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("Accept", "application/json").
		SetHeader("Authorization", r.Token).
		SetHeader("Accept-Language", "en")

	return req
}

func (r *RequestInfo) GetJenisRequest() map[string]interface{} {
	var res map[string]interface{}

	return res
}

func (r *RequestInfo) GetDetailRequest() map[string]interface{} {
	var res map[string]interface{}

	return res
}

	func (r *RequestInfo) SendTicket() error {

	return nil
}
