package main

import (
	"fmt"
	"log"
	"mymodule/model"
	"mymodule/pkg"

	"github.com/bxcodec/faker/v3"
	"github.com/go-pg/pg"
	"golang.org/x/crypto/bcrypt"
)

func main() {

	filePath := "./config.yaml"

	var cfg model.Config
	err := pkg.ReadFile(&cfg, filePath)
	if err != nil {
		fmt.Println("ERROR READ FILE")
		log.Fatal(err)
	}

	db := pg.Connect(&pg.Options{
		Addr:     fmt.Sprintf("%s:%d", cfg.DB.Address, cfg.DB.Port),
		Database: cfg.DB.Name,
		User:     cfg.DB.User,
		Password: cfg.DB.Password,
	})

	password := []byte(string("admin"))
	password, err = bcrypt.GenerateFromPassword(password, bcrypt.DefaultCost)
	if err != nil {
		fmt.Println("ERROR BYCRIPT")
		panic(err)
	}

	// roles := []model.Role{
	// 	{
	// 		Name:      "Direksi",
	// 		CreatedBy: "",
	// 	},
	// 	{
	// 		Name:      "Manager",
	// 		CreatedBy: "",
	// 	},
	// 	{
	// 		Name:      "Supervisor",
	// 		CreatedBy: "",
	// 	},
	// 	{
	// 		Name:      "Operator",
	// 		CreatedBy: "",
	// 	},
	// }

	// _, err = db.Model(&roles).Insert()

	// if err != nil {
	// 	fmt.Println("ERROR : ", err)
	// 	panic(err)
	// }

	_, err = db.Model(&model.User{
		Username: "admin",
		Password: string(password),
		Token:    "",
		Email:    faker.Email(),
		Info: model.Info{
			Name:               faker.Name(),
			Profile:            "",
			Role:               "admin",
			Department:         "IT",
			Level:              "admin",
			TokenGeneratedTime: 0,
		},
		CreatedBy: "",
	}).Insert()

	if err != nil {
		panic(err)
	}

	defer db.Close()

}
